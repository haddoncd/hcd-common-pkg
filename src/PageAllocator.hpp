#ifdef PageAllocator_hpp
#error Multiple inclusion
#endif
#define PageAllocator_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef HeapAllocator_hpp
#error "Please include HeapAllocator.hpp before this file"
#endif


struct PageAllocator
{
  struct Object
  {
    static constexpr size_t MIN_SIZE = (256 * SI_KIBI);

    Object *next;
    size_t  position;
    u8      memory[0];

    static size_t sizeWith(size_t size);
  };

  struct Page
  {
    static constexpr size_t SIZE     = (1 * SI_MEBI);
    static constexpr size_t CAPACITY = (SIZE - (2 * sizeof(void *)));

    Page   *next;
    Object *objects;
    u8      memory[CAPACITY];
  };

  static_assert(sizeof(Page) == Page::SIZE, "PageAllocator::Page::SIZE");
  static_assert(Object::MIN_SIZE < Page::CAPACITY, "PageAllocator::Object::MIN_SIZE");

  struct State
  {
    HeapAllocator *heap;
    Page          *page;
    u32            pageNum;
    Page          *freePages;
    u32            freePageCount;

    void init(HeapAllocator *heap = STD_HEAP);
    void destroy();
  };

  static constexpr u32 MAX_FREE_PAGES = UINT32_MAX;

  // WARNING: Takes "ownership" of the state, and assumes everything allocated
  // from it can be destroyed.
  static PageAllocator reuseState(State *state);

  State *state;
  u32    pageNum;
  u32    position;

  void init(HeapAllocator *heap = STD_HEAP);

  // WARNING: Assumes that this allocator was init()-ed, and as such frees the
  // contained state.
  void destroy();

  u8 *allocInternal(bool zero, size_t size, size_t alignment = 1);

  void clear();
};

u8 *allocRaw(PageAllocator *heap, size_t size, size_t alignment = 1);
u8 *allocUninitRaw(PageAllocator *heap, size_t size, size_t alignment = 1);
