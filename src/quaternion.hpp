#ifdef quaternion_hpp
#error Multiple inclusion
#endif
#define quaternion_hpp

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif

#ifndef matrix_hpp
#error "Please include matrix.hpp before this file"
#endif



// References:
//   https://www.3dgep.com/understanding-quaternions/
//   http://allenchou.net/2014/04/game-math-quaternion-basics/
//   http://number-none.com/product/Hacking%20Quaternions/index.html
//   http://number-none.com/product/Understanding%20Slerp,%20Then%20Not%20Using%20It/index.html

template <typename T>
struct Q
{
  union
  {
    struct
    {
      V3<T> v;
      T     s;
    };
    struct
    {
      T x, y, z, w;
    };
  };

  static constexpr Q<T> identity()
  {
    return q<T>(0, 0, 0, 1);
  }

  static Q<T> fromAxisAngle(V3<T> axis, T radians)
  {
    T     half_angle = radians * 0.5f;
    T sin_half_angle = sin(half_angle);
    T cos_half_angle = cos(half_angle);
    T inv_axis_magnitude = invSqrt(dot(axis, axis));

    return q<T>(axis * inv_axis_magnitude * sin_half_angle, cos_half_angle);
  }

  T magnitudeSquared()
  {
    return square(v.x) +
           square(v.y) +
           square(v.z) +
           square(s);
  }

  // M3<T> toM3()
  // {
  //   return m3<T>(w*w+x*x-y*y-z*z, 2*(-w*z+x*y),    2*(w*y+x*z),
  //                2*(w*z+x*y),     w*w-x*x+y*y-z*z, 2*(-w*x+y*z),
  //                2*(-w*y+x*z),    2*(w*x+y*z),     w*w-x*x-y*y+z*z) / w*w+x*x+y*y+z*z;
  // }

  M4<T> toM4()
  {
    return m4<T>(w*w+x*x-y*y-z*z, 2*(w*z+x*y),     2*(-w*y+x*z),    0,
                 2*(-w*z+x*y),    w*w-x*x+y*y-z*z, 2*(w*x+y*z),     0,
                 2*(w*y+x*z),     2*(-w*x+y*z),    w*w-x*x-y*y+z*z, 0,
                 0,               0,               0,               w*w+x*x+y*y+z*z);
  }

  // TODO normalization, interpolation
};

template <typename T>
Q<T> q(V3<T> v, T s)
{
  Q<T> result;
  result.v = v;
  result.s = s;
  return result;
}

template <typename T>
Q<T> q(T x, T y, T z, T w)
{
  Q<T> result;
  result.x = x;
  result.y = y;
  result.z = z;
  result.w = w;
  return result;
}

// NB. This quaternion implementation has the opposite handedness to the usual
// convention. q2 * q1 will rotate by q1 first, then q2.
// This is for consistency with the matrix implementation.
template <typename T>
Q<T> operator *(const Q<T> &a, const Q<T> &b)
{
  Q<T> result;
  result.v = (a.v * b.s) + (a.s * b.v) - cross(a.v, b.v);
  result.s = (a.s * b.s) - dot(a.v, b.v);

  // Confirm that we're normalised (within some arbitrarily chosen epsilon) to
  // remind me to implement normalisation before this blows up in my face.
  assert(absDiff(result.magnitudeSquared(), (T) 1) < ((T) 1 / (T) 1024));

  return result;
}

// NB. Due to the "backwards" handedness of our quaternion multiplication, operator*= is
// also backwards to cancel this, ie:
//   q *= q'"
// is equivalent to:
//   q = q' * q
// resulting in rotating by q' AFTER q.
template <typename T>
Q<T> &operator *=(Q<T> &a, const Q<T> &b)
{
  a = b * a;
  return a;
}
