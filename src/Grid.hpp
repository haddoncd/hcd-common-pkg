#ifdef Grid_hpp
#error Multiple inclusion
#endif
#define Grid_hpp

template <typename T>
struct pGrid;

template <typename T>
struct Grid
{
  V2<size_t> size;
  T          grid[0];

  NO_COPY_OR_MOVE(Grid)

  static size_t sizeWith(V2<size_t> size)
  {
    return offsetOf(Grid<T>, grid[size.x * size.y]);
  }

  static size_t sizeWith(size_t x, size_t y)
  {
    return sizeWith(v2<size_t>(x, y));
  }

  void setSize(V2<size_t> size)
  {
    this->size = size;
  }

  void setSize(size_t x, size_t y)
  {
    setSize(v2<size_t>(x, y));
  }

  template <typename A, typename... Us>
  static pGrid<T> alloc(A al, Us &&... args)
  {
    pGrid<T> result;
    result.p = allocWith<Grid<T>>(al, std::forward<Us>(args)...);
    return result;
  }

  template <typename A, typename... Us>
  static pGrid<T> allocUninit(A al, Us &&... args)
  {
    pGrid<T> result;
    result.p = allocUninitWith<Grid<T>>(al, std::forward<Us>(args)...);
    return result;
  }
};

template <typename T>
struct pGrid
{
  Grid<T> *p;

  T *operator[](size_t idx)
  {
    assert(idx < p->size.x);
    return &p->grid[idx * p->size.y];
  }

  V2<size_t> size()
  {
    return p->size;
  }

  template <typename U>
  T &operator[](V2<U> v)
  {
    assert(inBounds(v));
    return p->grid[(v.x * p->size.y) + v.y];
  }

  template <typename U>
  bool inBounds(V2<U> pos)
  {
    return 0 <= pos.x && pos.x < p->size.x &&
           0 <= pos.y && pos.y < p->size.y;
  }

  void zero()
  {
    zeroArray(p->grid, p->size.x * p->size.y);
  }

  template <typename A>
  void heapFree(A al)
  {
    heapFree(al, p);
    p = 0;
  }
};
