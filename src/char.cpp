#ifdef char_cpp
#error Multiple inclusion
#endif
#define char_cpp

#ifndef char_hpp
#include "char.hpp"
#endif

bool char_isLowerAlpha(char c)
{
  return 'a' <= c && c <= 'z';
}

bool char_isUpperAlpha(char c)
{
  return 'A' <= c && c <= 'Z';
}

bool char_isAlpha(char c)
{
  return char_isLowerAlpha(c) || char_isUpperAlpha(c);
}
