#ifdef path_hpp
#error Multiple inclusion
#endif
#define path_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

namespace path
{
  template <typename A>
  Str workingDir(A allocator)
  {
    Str result;

    u32 buf_size = GetCurrentDirectoryA(0, 0);
    char *buf = allocArray<char>(allocator, buf_size);
    result.data = buf;
    result.length = GetCurrentDirectoryA(buf_size, buf);

    return result;
  }

  namespace Type
  {
    enum Enum
    {
      INVALID,
      ABS_FWD,
      ABS_BACK,
      REL_FWD,
      REL_BACK,
      REL_UNKNOWN,
      ENUM_COUNT
    };

    extern bool isAbs[ENUM_COUNT];

    extern bool isRel[ENUM_COUNT];

    extern bool usesFwdSlash[ENUM_COUNT];

    extern bool usesBackSlash[ENUM_COUNT];
  }

  Type::Enum getType(Str path);
}
