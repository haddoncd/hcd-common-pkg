#ifdef print_hpp
#error Multiple inclusion
#endif
#define print_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef StrBuf_hpp
#error "Please include StrBuf.hpp before this file"
#endif



/////////////////////////////////////
// variadic template chaining etc. //
/////////////////////////////////////

template <typename S, typename T1, typename T2, typename ...Ts>
bool print(S *out, T1 &&t1, T2 &&t2, Ts &&... args)
{
  static_assert(!TypesAreAllStringLiteral<T1, T2>, "Performance bug - print statement contains adjacent string literals. Omit the comma to have them concatenated by the preprocessor!");
  return print(out, std::forward<T1>(t1)) && print(out, std::forward<T2>(t2), std::forward<Ts>(args)...);
}

template <typename ...Ts>
bool println(Ts &&... args)
{
  return print(std::forward<Ts>(args)..., '\n');
}

template <typename ...Ts>
u64 printSize(Ts &&... args)
{
  StrBuf::Null buf;
  print(&buf, std::forward<Ts>(args)...);
  buf.undoNullTerminate();
  return buf.size;
}

template <typename A, typename ...Ts>
StrBuf::Variable *sprint(A allocator, Ts &&... args)
{
  u32 length = castOrAssert<u32>(printSize(std::forward<Ts>(args)...) + 1);
  auto *buf = allocUninitWith<StrBuf::Variable>(allocator, length);
  buf->clear();
  print(buf, std::forward<Ts>(args)...);
  buf->nullTerminate();
  return buf;
}

#define sprintLocal(p, ...)                                   \
do                                                            \
{                                                             \
  u32 length = castOrAssert<u32>(printSize(__VA_ARGS__) + 1); \
  StrBuf_Variable_localAllocAndInit(p, length);               \
  print(p, __VA_ARGS__);                                      \
}                                                             \
while (false)


////////////////////
// string literal //
////////////////////

template <size_t L>
bool print(FILE *out, StringLiteral<L> string)
{
  size_t length = L - 1;
  return fwrite(string, sizeof(char), length, out) == length;
}

template <typename B, size_t L>
bool print(B *out, StringLiteral<L> string)
{
  return out->append(string);
}


/////////
// Str //
/////////

bool print(FILE *out, Str str);

template <typename B>
bool print(B *out, Str str)
{
  return out->append(str);
}


//////////
// char //
//////////

bool print(FILE *out, char c);

template <typename B>
bool print(B *out, char c)
{
  return out->append(c);
}


////////////////////
// repeated items //
////////////////////

template <typename T>
struct RepeatPrintCommand
{
  u32 count;
  T   item;
};

template <typename T>
RepeatPrintCommand<T> repeat(u32 count, T item)
{
  return RepeatPrintCommand<T> { count, item };
}

template <typename T>
bool print(FILE *out, RepeatPrintCommand<T> cmd)
{
  bool ok = true;
  for (u32 i = 0; ok && (i < cmd.count); ++i) ok &= print(out, cmd.item);
  return ok;
}

template <typename T, typename B>
bool print(B *out, RepeatPrintCommand<T> cmd)
{
  bool ok = true;
  for (u32 i = 0; ok && (i < cmd.count); ++i) ok &= print(out, cmd.item);
  return ok;
}


////////////
// printf //
////////////

namespace MultiValuePrintfCommand
{
  template <typename F>
  struct Command
  {
    F print;
    Command(F print) : print(print) {}
  };

  template <typename F>
  Command<F> fromLambda(F print)
  {
    return Command<F>(print);
  };
}

template <typename... Ts>
auto fmt(char *format, const Ts &... args)
{
  return MultiValuePrintfCommand::fromLambda([=](FILE *stream, StrBuf::Fixed<0> *buf, u32 buf_capacity, StrBuf::Null *null_buf)
  {
    assert(boolXor(boolXor(stream, buf), null_buf));

    if         (stream)     return 0 <= fprintf(stream, format, args...);
    else    if (buf)        return StrBuf::Generic::append(buf, buf_capacity, format, args...);
    else /* if (nul_buf) */ return null_buf->append(format, args...);
  });
}

template <typename F>
bool print(FILE *stream, MultiValuePrintfCommand::Command<F> cmd)
{
  return cmd.print(stream, 0, 0, 0);
}

template <u64 N, typename F>
bool print(StrBuf::Fixed<N> *buf, MultiValuePrintfCommand::Command<F> cmd)
{
  return cmd.print(0, (StrBuf::Fixed<0> *)buf, N, 0);
}

template <typename F>
bool print(StrBuf::Variable *buf, MultiValuePrintfCommand::Command<F> cmd)
{
  return cmd.print(0, &buf->generic, buf->capacity, 0);
}

template <typename F>
bool print(StrBuf::Null *null_buf, MultiValuePrintfCommand::Command<F> cmd)
{
  return cmd.print(0, 0, 0, null_buf);
}



////////////////////////////////////
// printf formatted single values //
////////////////////////////////////

template <typename T> struct PrintfType;

#define DeclarePrintfType(type) \
  template <> struct PrintfType<type>       \
  {                                         \
    static const ZStr DEFAULT_FORMAT;       \
    static const Str SYMBOL;                \
  };

#define DefinePrintfType(type, conversion)                            \
  const ZStr PrintfType<type>::DEFAULT_FORMAT = "%" ZSTR(conversion); \
  const Str PrintfType<type>::SYMBOL =                                \
    PrintfType<type>::DEFAULT_FORMAT.s.trimLeft(1);

DeclarePrintfType(u32)
DeclarePrintfType(i32)
DeclarePrintfType(u64)
DeclarePrintfType(i64)
DeclarePrintfType(f64)

namespace SingleValuePrintfCommand
{
  template <typename T>
  bool printf(FILE *out, const char *format, T value)
  {
    return 0 <= fprintf(out, format, value);
  }

  template <typename B, typename T>
  bool printf(B *out, const char *format, T value)
  {
    return out->append(format, value);
  }

  template <typename T>
  struct Default
  {
    T value;

    template <typename S>
    bool printTo(S out)
    {
      return printf(out, PrintfType<T>::DEFAULT_FORMAT, value);
    }
  };

  template <typename T>
  struct Modified
  {
    T   value;
    Str modifiers;

    template <typename S>
    bool printTo(S out)
    {
      StrBuf::Variable *format;
      sprintLocal(format, "%"_s, modifiers, PrintfType<T>::SYMBOL);
      return printf(out, format->zStr(), value);
    }
  };
}

template <typename T>
SingleValuePrintfCommand::Default<T> fmt(T value)
{
  return SingleValuePrintfCommand::Default<T> { value };
}

template <typename T, size_t L>
SingleValuePrintfCommand::Modified<T> fmt(T value, StringLiteral<L> modifiers)
{
  return SingleValuePrintfCommand::Modified<T> { value, Str::fromLiteral(modifiers) };
}

template <typename S, typename T>
bool print(S out, SingleValuePrintfCommand::Default<T> cmd)
{
  return cmd.printTo(out);
}

template <typename S, typename T>
bool print(S out, SingleValuePrintfCommand::Modified<T> cmd)
{
  return cmd.printTo(out);
}


//////////////
// pointers //
//////////////

struct PrintPointerCommand
{
  uptr p;
};

template <typename T>
PrintPointerCommand fmt(T *p)
{
  return PrintPointerCommand{(uptr)p};
}

template <typename S>
bool print(S out, PrintPointerCommand cmd)
{
  static char base64[65] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?@";

  bool error = false;

  BREAKABLE_START
  {
    bool latch = false;
    for (u32 i = 0; i < 8; ++i)
    {
      u64 idx = (cmd.p >> (58 - (6 * i))) & 63;

      if (idx || latch || i == 7)
      {
        latch = true;
        if (!print(out, fmt("%c", base64[idx])))
        {
          error = true;
          break;
        }
      }
    }
    if (error) break;


    if (!print(out, "-"_s))
    {
      error = true;
      break;
    }

    for (u32 i = 0; i < 4; ++i)
    {
      u64 idx = (cmd.p >> (12 - (4 * i))) & 15;

      if (!print(out, fmt("%c", base64[idx])))
      {
        error = true;
        break;
      }
    }
    if (error) break;
  }
  BREAKABLE_END

  return !error;
}
