#ifdef common_cpp
#error Multiple inclusion
#endif
#define common_cpp

#ifndef common_hpp
#include "common.hpp"
#endif

template <> void assertRepresentableAs<u32, i32>(i32 x) { assert(0 <= x); }
template <> void assertRepresentableAs<u32, u64>(u64 x) { assert(x <= UINT32_MAX); }
template <> void assertRepresentableAs<i32, u64>(u64 x) { assert(x <= INT32_MAX); }
template <> void assertRepresentableAs<i16, f32>(f32 x) { assert(INT16_MIN <= x && x <= INT16_MAX && x == (i16)x); }
template <> void assertRepresentableAs<i16, i32>(i32 x) { assert(INT16_MIN <= x && x <= INT16_MAX); }
