#ifdef Str_cpp
#error Multiple inclusion
#endif
#define Str_cpp

#ifndef Str_hpp
#include "Str.hpp"
#endif



Str Str::slice(u64 start, u64 span) const
{
  return start < length ?
         Str{ length < start + span ?
              length - start : span,
              data + start } :
         Str::empty();
}

Str Str::left(u64 span) const
{
  return Str{minimum(span, length), data};
}

Str Str::trimLeft(u64 span) const
{
  return span < length ? Str{length - span, data + span} : Str::empty();
}

Str Str::right(u64 span) const
{
  return (length <= span) ? *this : Str{span, data + (length - span)};
}

Str Str::trimRight(u64 span) const
{
  return span < length ? Str{length - span, data} : Str::empty();
}

bool Str::operator==(const Str &that) const
{
  return length != that.length              ? false :
         (length == 0 || data == that.data) ? true  :
         memcmp(data, that.data, length) == 0;
}

bool Str::operator!=(const Str &that) const
{
  return !(*this == that);
}

bool Str::beginsWith(Str prefix) const
{
  return (prefix.length <= length) &&
         (left(prefix.length) == prefix);
}

bool Str::endsWith(Str suffix) const
{
  return (suffix.length <= length) &&
         (right(suffix.length) == suffix);
}

u64 Str::find(Str needle) const
{
  u64 result = Str::NOT_FOUND;

  if (needle.length == 0)
  {
    result = 0;
  }
  else if (needle.length <= length)
  {
    for (u64 i = 0; i <= length - needle.length; ++i)
    {
      if (memcmp(data + i, needle.data, needle.length) == 0)
      {
        result = i;
        break;
      }
    }
  }

  return result;
}

u64 Str::findReverse(Str needle) const
{
  u64 result = Str::NOT_FOUND;

  if (needle.length == 0)
  {
    result = 0;
  }
  else if (needle.length <= length)
  {
    for (u64 i = length - needle.length; i < length; --i)
    {
      if (memcmp(data + i, needle.data, needle.length) == 0)
      {
        result = i;
        break;
      }
    }
  }

  return result;
}

u64 Str::count(char needle) const
{
  u64 result = 0;
  for (u64 i = 0; i < length; ++i)
  {
    if (data[i] == needle)
    {
      ++result;
    }
  }
  return result;
}

Str Str::chop(Str delim)
{
  u64 delim_index = find(delim);
  Str result;

  if (delim_index != Str::NOT_FOUND)
  {
    result = Str{delim_index, data};
    length -= delim_index + delim.length;
    data   += delim_index + delim.length;
  }
  else
  {
    result = *this;
    *this = Str::empty();
  }

  return result;
}

Str Str::truncatedAtFirst(Str delim) const
{
  return Str{minimum(length, find(delim)),
             data};
}

Str Str::truncatedAtLast(Str delim) const
{
  return Str{minimum(length, findReverse(delim)),
             data};
}

// TODO: Include a strtod implementation which doesn't expect null termination.
f64 Str::parseF64() const
{
  ZStr zstr;
  ZStr_localAllocCopy(zstr, *this);
  return zstr.parseF64();
}

i32 compare(Str a, Str b)
{
  i32 result = memcmp(a.data, b.data, minimum(a.length, b.length));
  if (!result) result = compare(a.length, b.length);
  return result;
}

constexpr ZStr ZStr::empty()
{
  return ZStr{{0, &empty_null_terminator}};
}

ZStr::operator Str() const
{
  return s;
}

ZStr::operator char *() const
{
  return (char *)s.data;
}

ZStr::operator const char *() const
{
  return (const char *)s.data;
}

bool ZStr::operator==(const Str &that) const
{
  return that == *this;
}

bool ZStr::operator!=(const Str &that) const
{
  return !(that == *this);
}

// TODO: Include a strtod implementation which doesn't expect null termination (and then remove this!)
f64 ZStr::parseF64() const
{
  char *end;
  f64 result = strtod(s.data, &end);
  if (end == s.data) result = F64_NAN;

  return result;
}

constexpr ZStr operator"" _s(const char *data, size_t length)
{
  return ZStr{{length, data}};
}

ZStr zStr(const char *data)
{
  return data ? ZStr{ strlen(data), (const char *)data } : ZStr::empty();
}
