#ifdef HeapAllocator_cpp
#error Multiple inclusion
#endif
#define HeapAllocator_cpp

#ifndef HeapAllocator_hpp
#include "HeapAllocator.hpp"
#endif


HeapAllocator STD_HEAP_STRUCT = { malloc, calloc, realloc, free };
HeapAllocator *STD_HEAP = &STD_HEAP_STRUCT;

u8 *allocRaw(HeapAllocator *heap, size_t size, size_t ignored)
{
  return (u8 *)heap->calloc(1, size);
}

u8 *allocUninitRaw(HeapAllocator *heap, size_t size, size_t ignored)
{
  return (u8 *)heap->malloc(size);
}
