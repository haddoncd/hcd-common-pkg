#ifdef path_cpp
#error Multiple inclusion
#endif
#define path_cpp

#ifndef path_hpp
#include "path.hpp"
#endif

#ifndef char_hpp
#error "Please include char.hpp before this file"
#endif

// TODO: Non Win32 platforms
#ifndef _WINDOWS_
#error "Please include Windows.h before this file"
#endif



namespace path
{
  namespace Type
  {
    bool isAbs[ENUM_COUNT] =
    {
      false,
      true,
      true,
      false,
      false,
      false
    };

    bool isRel[ENUM_COUNT] =
    {
      false,
      false,
      false,
      true,
      true,
      true
    };

    bool usesFwdSlash[ENUM_COUNT] =
    {
      false,
      true,
      false,
      true,
      false,
      false
    };

    bool usesBackSlash[ENUM_COUNT] =
    {
      false,
      false,
      true,
      false,
      true,
      false
    };
  }

  Type::Enum getType(Str path)
  {
    Type::Enum result;

    if (!path.length)
    {
      result = Type::INVALID;
    }
    else
    {
      size_t first_fwd_slash = path.find('/');
      size_t first_back_slash = path.find('\\');

      if (first_fwd_slash != Str::NOT_FOUND)
      {
        if (first_back_slash != Str::NOT_FOUND)
        {
          result = Type::INVALID;
        }
        else if ( first_fwd_slash == 0 ||
                  ( char_isAlpha(path.data[0]) &&
                    path.slice(1, 2) == ":/"_s ) )
        {
          result = Type::ABS_FWD;
        }
        else
        {
          result = Type::REL_FWD;
        }
      }
      else
      {
        if (first_back_slash == Str::NOT_FOUND)
        {
          result = Type::REL_UNKNOWN;
        }
        else if ( first_back_slash == 0 ||
                  ( char_isAlpha(path.data[0]) &&
                    path.slice(1, 2) == ":\\"_s ) )
        {
          result = Type::ABS_BACK;
        }
        else
        {
          result = Type::REL_BACK;
        }
      }
    }

    return result;
  }
}
