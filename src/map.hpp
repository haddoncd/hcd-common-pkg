#ifdef map_hpp
#error Multiple inclusion
#endif
#define map_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

namespace map
{
  // NB. No map is allowed to be large enough to contain a bucket at this index.
  static constexpr u32 INVALID_IDX = UINT32_MAX;

  static constexpr f32 DEFAULT_OCCUPANCY = 0.90f;

  // Returns UINT32_MAX to indicate failure
  constexpr u32 chooseSize(u32 capacity, f32 occupancy = DEFAULT_OCCUPANCY)
  {
    // TODO: Proper ceil() - i.e. don't just add 0.5 and cast!
    return (0 < capacity) && (0.0f < occupancy) && (occupancy < 1.0f)
      ? ceilPowerOfTwo((u32)((capacity / occupancy) + 0.5))
      : UINT32_MAX;
  }

  template <typename K, typename V>
  struct Bucket
  {
    // Bucket ordinal:
    //   0 => empty
    //   N => the key stored here is in it's Nth preferred bucket
    u32 ordinal;

    K   key;
    V   value;
  };

  namespace generic
  {
    template <typename M>
    u32 getIdx(M *map, u32 ideal, u32 ordinal)
    {
      assert(ideal < map->mapSize());
      assert(1 <= ordinal);
      assert(ordinal <= map->mapSize());
      u32 result = (ideal + ordinal - 1) & map->idxMask();
      return result;
    }

    template <typename M>
    u32 getIdeal(M *map, u32 idx, u32 ordinal)
    {
      assert(idx < map->mapSize());
      assert(1 <= ordinal);
      assert(ordinal <= map->mapSize());
      u32 result = (idx + 1 + map->mapSize() - ordinal) & map->idxMask();
      return result;
    }

    template <typename M>
    u32 getOrdinal(M *map, u32 ideal, u32 idx)
    {
      assert(ideal < map->mapSize());
      assert(idx < map->mapSize());
      u32 result = ((idx + map->mapSize() - ideal) & map->idxMask()) + 1;
      return result;
    }

    struct SearchResult
    {
      enum Type
      {
        MATCH,
        EMPTY,
        LATER
      };

      Type type;
      u32 idx;
    };

    template <typename M>
    SearchResult probe(M *map, u32 ideal, u32 ordinal = 1)
    {
      SearchResult result;

      for (;; ++ordinal)
      {
        u32 idx = getIdx(map, ideal, ordinal);

        if (map->table[idx].ordinal == 0)
        {
           result = SearchResult { SearchResult::EMPTY, idx };
           break;
        }
        else if (map->table[idx].ordinal < ordinal)
        {
           result = SearchResult { SearchResult::LATER, idx };
           break;
        }
      }

      return result;
    }

    template <typename M>
    SearchResult probe(M *map, typename M::Key key, u32 ideal, u32 ordinal = 1)
    {
      SearchResult result;

      for (;; ++ordinal)
      {
        u32 idx = getIdx(map, ideal, ordinal);

        if (map->table[idx].ordinal == ordinal &&
            map->table[idx].key     == key)
        {
           result = SearchResult { SearchResult::MATCH, idx };
           break;
        }
        else if (map->table[idx].ordinal == 0)
        {
           result = SearchResult { SearchResult::EMPTY, idx };
           break;
        }
        else if (map->table[idx].ordinal < ordinal)
        {
           result = SearchResult { SearchResult::LATER, idx };
           break;
        }
      }

      return result;
    }

    template <bool FAIL_IF_FOUND, bool CREATE_IF_NOT_FOUND, typename M>
    u32 find(M *map, typename M::Key key, bool *created_out = 0)
    {
      u32 result;

      u32 ideal_idx = map->idealIdx(key);
      SearchResult sr = probe(map, key, ideal_idx);

      switch (sr.type)
      {
        case SearchResult::LATER:
        {
          if (CREATE_IF_NOT_FOUND && map->occupancy < map->mapSize())
          {
            typename M::Bucket displaced = map->table[sr.idx];
            u32 first_displaced_ideal = getIdeal(map, sr.idx, displaced.ordinal);
            SearchResult dsr = probe(map, first_displaced_ideal, displaced.ordinal + 1);

            u32 subsequent_displaced_ideal = first_displaced_ideal;
            while (dsr.type == SearchResult::LATER)
            {
              displaced.ordinal = getOrdinal(map, subsequent_displaced_ideal, dsr.idx);
              swap(map->table[dsr.idx], displaced);
              subsequent_displaced_ideal = getIdeal(map, dsr.idx, displaced.ordinal);
              dsr = probe(map, subsequent_displaced_ideal, displaced.ordinal + 1);
            }
            assert(dsr.type == SearchResult::EMPTY);

            map->table[dsr.idx].ordinal = getOrdinal(map, first_displaced_ideal, dsr.idx);
            map->table[dsr.idx].key     = displaced.key;
            map->table[dsr.idx].value   = displaced.value;
          }
        } // fallthrough

        case SearchResult::EMPTY:
        {
          if (CREATE_IF_NOT_FOUND && map->occupancy < map->mapSize())
          {
            map->table[sr.idx].ordinal = getOrdinal(map, ideal_idx, sr.idx);
            map->table[sr.idx].key     = key;
            ++map->occupancy;
            result = sr.idx;
            if (created_out) *created_out = true;
          }
          else
          {
            result = INVALID_IDX;
          }
        } break;

        case SearchResult::MATCH:
        {
          if (FAIL_IF_FOUND)
          {
            result = INVALID_IDX;
          }
          else
          {
            result = sr.idx;
            if (created_out) *created_out = false;
          }
        } break;

        default:
          assert(false);
          result = INVALID_IDX;
      }

      return result;
    }

    template <typename M>
    void removeAtIdx(M *map, u32 idx)
    {
      while (true)
      {
        u32 next_idx = idx + 1;
        next_idx &= map->idxMask();

        if (map->table[next_idx].ordinal <= 1) break;

        map->table[idx] = map->table[next_idx];
        idx = next_idx;
      }

      map->table[idx].ordinal = 0;
      --map->occupancy;
    }

    template <typename M>
    bool add(M *map, typename M::Key key, typename M::Value value)
    {
      bool result = false;

      u32 idx = generic::find<true, true>(map, key);
      if (idx != INVALID_IDX)
      {
        map->table[idx].value = value;
        result = true;
      }

      return result;
    }

    template <typename M>
    typename M::Value *putPtr(M *map, typename M::Key key, bool *created_out)
    {
      typename M::Value * result = 0;

      u32 idx = generic::find<false, true>(map, key, created_out);
      if (idx != INVALID_IDX)
      {
        result = &map->table[idx].value;
      }

      return result;
    }

    template <typename M>
    bool put(M *map, typename M::Key key, typename M::Value value, bool *created_out)
    {
      bool result = false;

      u32 idx = generic::find<false, true>(map, key, created_out);
      if (idx != INVALID_IDX)
      {
        map->table[idx].value = value;
        result = true;
      }

      return result;
    }

    template <typename M>
    typename M::Value *getPtr(M *map, typename M::Key key)
    {
      typename M::Value *result = 0;

      u32 idx = generic::find<false, false>(map, key);
      if (idx != INVALID_IDX) result = &map->table[idx].value;

      return result;
    }

    template <typename M>
    typename M::Value get(M *map, typename M::Key key, typename M::Value default_value)
    {
      typename M::Value result;

      u32 idx = generic::find<false, false>(map, key);
      if (idx != INVALID_IDX)
      {
        result = map->table[idx].value;
      }
      else
      {
        result = default_value;
      }

      return result;
    }

    template <typename M>
    bool tryGet(M *map, typename M::Key key, typename M::Value *value_out)
    {
      bool result = false;

      u32 idx = generic::find<false, false>(map, key);
      if (idx != INVALID_IDX)
      {
        *value_out = map->table[idx].value;
        result = true;
      }

      return result;
    }

    template <typename M>
    bool contains(M *map, typename M::Key key)
    {
      bool result;

      u32 idx = generic::find<false, false>(map, key);
      result = (idx != INVALID_IDX);

      return result;
    }

    template <typename M>
    bool remove(M *map, typename M::Key key, typename M::Value *value_out)
    {
      bool result = false;

      u32 idx = generic::find<false, false>(map, key);
      if (idx != INVALID_IDX)
      {
        if (value_out) *value_out = map->table[idx].value;
        removeAtIdx(map, idx);
        result = true;
      }

      return result;
    }

    template <typename M>
    void clear(M *map)
    {
      map->occupancy = 0;
      for (u32 i = 0; i < map->mapSize(); ++i)
      {
        map->table[i].ordinal = 0;
      }
    }
  }

  template <typename K, typename V, u32 (*F)(K), u32 S>
  struct Fixed
  {
    static_assert(0 < S, "Map size must be positive");
    static_assert(isPowerOfTwo(S),"Map size must be power of two");
    static_assert(S - 1 < INVALID_IDX,
      "Map size must not be large enough to contain INVALID_IDX");

    typedef K Key;
    typedef V Value;
    typedef Bucket<K,V> Bucket;

    static constexpr u32 (*idealIdx)(K) = F;

    static constexpr u32 hashSize() { return u32Log2(S); }

    static constexpr u32 idxMask()  { return S - 1; }

    static constexpr u32 mapSize()  { return S; }

    u32 occupancy;
    Bucket table[S];

    bool add(K key, V value)
    {
      return generic::add(this, key, value);
    }

    V *putPtr(K key, bool *created_out = 0)
    {
      return generic::putPtr(this, key, created_out);
    }

    bool put(K key, V value, bool *created_out = 0)
    {
      return generic::put(this, key, value, created_out);
    }

    V *getPtr(K key)
    {
      return generic::getPtr(this, key);
    }

    V get(K key, V default_value)
    {
      return generic::get(this, key, default_value);
    }

    bool tryGet(K key, V *value_out)
    {
      return generic::tryGet(this, key, value_out);
    }

    bool contains(K key)
    {
      return generic::contains(this, key);
    }

    bool remove(K key, V *value_out = 0)
    {
      return generic::remove(this, key, value_out);
    }

    void clear()
    {
      generic::clear(this);
    }
  };

  template <typename K, typename V, u32 (*F)(u8, K)>
  struct Variable
  {
    typedef K Key;
    typedef V Value;
    typedef Bucket<K,V> Bucket;

    u8 hashSize_;
    u32 occupancy;
    Bucket table[];

    static constexpr size_t sizeWith(u32 size)
    {
      typedef Variable<K,V,F> Map;
      return offsetOf(Map, table[size]);
    }

    NO_COPY_OR_MOVE(Variable)

    // Map must always be initialised, regardless of whether memory is zeroed.
    // If not zeroed, must additionally call clear().
    void init(u32 size)
    {
      // Map size must be positive
      assert(0 < size);
      // Map size must be power of two
      assert(isPowerOfTwo(size));
      // Map size must not be large enough to contain INVALID_IDX
      assert(size - 1 < INVALID_IDX);

      hashSize_ = u32Log2(size);
    }

    u32 hashSize()
    {
      return hashSize_;
    }

    u32 mapSize()
    {
      return 1u << hashSize();
    }

    u32 idxMask()
    {
      return mapSize() - 1;
    }

    u32 idealIdx(K key)
    {
      return F(hashSize(), key);
    }

    bool add(K key, V value)
    {
      return generic::add(this, key, value);
    }

    V *putPtr(K key, bool *created_out = 0)
    {
      return generic::putPtr(this, key, created_out);
    }

    bool put(K key, V value, bool *created_out = 0)
    {
      return generic::put(this, key, value, created_out);
    }

    V *getPtr(K key)
    {
      return generic::getPtr(this, key);
    }

    V get(K key, V default_value)
    {
      return generic::get(this, key, default_value);
    }

    bool tryGet(K key, V *value_out)
    {
      return generic::tryGet(this, key, value_out);
    }

    bool contains(K key)
    {
      return generic::contains(this, key);
    }

    bool remove(K key, V *value_out = 0)
    {
      return generic::remove(this, key, value_out);
    }

    void clear()
    {
      generic::clear(this);
    }
  };
}
