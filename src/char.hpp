#ifdef char_hpp
#error Multiple inclusion
#endif
#define char_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

bool char_isLowerAlpha(char c);
bool char_isUpperAlpha(char c);
bool char_isAlpha(char c);
