#ifdef Prng_cpp
#error Multiple inclusion
#endif
#define Prng_cpp

#ifndef Prng_hpp
#include "Prng.hpp"
#endif



// Xorshift algorithm by George Marsaglia
u32 Prng::next()
{
  u32 t = this->x ^ (this->x << 11);
  this->x = this->y;
  this->y = this->z;
  this->z = this->w;
  this->w ^= (this->w >> 19) ^ t ^ (t >> 8);
  return this->w;
}

u32 Prng::nextU32()
{
  return this->next();
}

u32 Prng::nextU32(u32 max)
{
  return this->next() % max;
}

u32 Prng::nextU32(u32 a, u32 b)
{
  u32 min = minimum(a, b);
  u32 max = maximum(a, b);
  u32 range = 1 + (max - min);

  return min + this->nextU32(range);
}

i32 Prng::nextI32(i32 a, i32 b)
{
  i32 min = minimum(a, b);
  i32 max = maximum(a, b);
  u32 range = 1 + (max - min);

  return min + this->nextU32(range);
}

f32 Prng::nextF32(f32 a, f32 b)
{
  f32 result = b + (this->next() * ((a - b) / (f32) UINT32_MAX));
  return result;
}

bool Prng::nextBool(f32 p)
{
  bool result = this->next() < (p * (((f32) UINT32_MAX)+1));
  return result;
}
