#ifdef args_cpp
#error Multiple inclusion
#endif
#define args_cpp

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef args_hpp
#include "args.hpp"
#endif



namespace args
{
  Type::Enum typeOf(Str arg)
  {
    using namespace Type;

    Enum result;

    if (arg.length == 0)
    {
      result = NON_OPT;
    }
    else if (arg.data[0] != '-')
    {
      result = NON_OPT;
    }
    else if (arg.length == 1)
    {
      result = NON_OPT;
    }
    else if (arg.data[1] != '-')
    {
      result = SHORT_OPTS;
    }
    else if (arg.length == 2)
    {
      result = END_OF_OPTS;
    }
    else
    {
      result = LONG_OPT;
    }

    return result;
  }

  size_t Array::sizeWith(u32 count)
  {
    return offsetOf(Array, args[count]);
  }

  void Array::init(u32 argc, char **argv)
  {
    assert(count == argc);

    bool end_of_opts = false;

    for (u32 i = 0; i < argc; ++i)
    {
      Str arg_str = zStr(argv[i]);

      if (!end_of_opts)
      {
        args[i].type = typeOf(arg_str);

        switch (args[i].type)
        {
          case Type::END_OF_OPTS:
            end_of_opts = true;
            args[i].arg = Str::empty();
            break;

          case Type::SHORT_OPTS:
            args[i].arg = arg_str.trimLeft(1);
            break;

          case Type::LONG_OPT:
            args[i].arg = arg_str.trimLeft(2);
            break;

          default:
            args[i].arg = arg_str;
            break;
        }
      }
      else
      {
        args[i].type = Type::NON_OPT;
        args[i].arg  = arg_str;
      }
    }
  }

  Iterator Array::start()
  {
    return Iterator{ this, 0, 0 };
  }

  Iterator Array::skipArg0()
  {
    return Iterator{ this, 1, 0 };
  }

  bool Iterator::valid()
  {
    return (argIndex < array->count);
  }

  void Iterator::step()
  {
    assert(argIndex < array->count);

    ArrayEntry *current_arg = &array->args[argIndex];

    if (current_arg->type == Type::SHORT_OPTS)
    {
      ++shortOptIndex;

      if (current_arg->arg.length <= shortOptIndex)
      {
        shortOptIndex = 0;
        ++argIndex;
      }
    }
    else
    {
      assert(shortOptIndex == 0);
      ++argIndex;
    }

    if (argIndex < array->count &&
        array->args[argIndex].type == Type::END_OF_OPTS)
    {
      ++argIndex;
    }
  }

  Str Iterator::getParam()
  {
    Str result = Str::empty();

    assert(argIndex < array->count);

    ArrayEntry *current_arg = &array->args[argIndex];

    switch (current_arg->type)
    {
      case Type::LONG_OPT:
      {
        u64 equals_index = current_arg->arg.find("=");
        if (equals_index == Str::NOT_FOUND) break;
        result = current_arg->arg.trimLeft(equals_index + 1);
      } break;

      case Type::SHORT_OPTS:
      {
        if ((current_arg->arg.length == shortOptIndex + 1) &&
            (argIndex + 1 < array->count) &&
            (array->args[argIndex + 1].type == Type::NON_OPT))
        {
          shortOptIndex = 0;
          ++argIndex;

          result = array->args[argIndex].arg;
        }
      } break;
    }

    return result;
  }

  Type::Enum Iterator::getType()
  {
    return array->args[argIndex].type;
  }

  Str Iterator::getArg()
  {
    Str result = array->args[argIndex].arg;

    if (array->args[argIndex].type == Type::SHORT_OPTS)
    {
      result = result.slice(shortOptIndex, 1);
    }

    return result;
  }

  bool Iterator::matchOpt(Str long_opt, char short_opt)
  {
    bool result = false;

    ArrayEntry *arg = &array->args[argIndex];

    if (arg->type == Type::LONG_OPT)
    {
      result = arg->arg.truncatedAtFirst("=") == long_opt;
    }
    else if (short_opt != '\0' &&
             arg->type == Type::SHORT_OPTS)
    {
      result = arg->arg.data[shortOptIndex] == short_opt;
    }

    return result;
  }
}


