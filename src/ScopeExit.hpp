#ifdef ScopeExit_hpp
#error Multiple inclusion
#endif
#define ScopeExit_hpp

template <typename F>
struct ScopeExit {
    F f;
    ScopeExit(F f) : f(f) {}
    ~ScopeExit() { f(); }
};

template <typename F>
ScopeExit<F> ScopeExit_fromLambda(F f) {
    return ScopeExit<F>(f);
};

// Indirection here allows preprocessor macros (eg. __LINE__) to be evaluated before concatenation
#define ScopeExit_preprocessorConcatenate(arg1, arg2) arg1 ## arg2
#define ScopeExit_indirectPreprocessorConcatenate(arg1, arg2) ScopeExit_preprocessorConcatenate(arg1, arg2)

#define SCOPE_EXIT(code) auto ScopeExit_indirectPreprocessorConcatenate(ScopeExit_, __LINE__) = ScopeExit_fromLambda([&](){code;});
