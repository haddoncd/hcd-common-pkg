#ifdef Hash_cpp
#error Multiple inclusion
#endif
#define Hash_cpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#include <type_traits>

#ifndef Hash_hpp
#include "Hash.hpp"
#endif

// FNV-1a hash, see http://www.isthe.com/chongo/tech/comp/fnv/

Hash32 Hash32::hash(u8 data) const
{
  return Hash32 { (value ^ data) * PRIME };
}

Hash32 Hash32::hash(u8 * data, u32 size) const
{
  Hash32 result = *this;

  for (u32 i = 0; i < size; ++i)
  {
    result = result.hash(data[i]);
  }

  return result;
}


Hash64 Hash64::hash(u8 data) const
{
  return Hash64 { (value ^ data) * PRIME };
}

Hash64 Hash64::hash(u8 * data, u32 size) const
{
  Hash64 result = *this;

  for (u32 i = 0; i < size; ++i)
  {
    result = result.hash(data[i]);
  }

  return result;
}


u32 Hash32::fold(u8 hash_size)
{
  assert(hash_size <= 32);

  u32 result;

  if (hash_size == 32)
  {
    result = value;
  }
  else
  {
    u32 mask = (1u << hash_size) - 1;
    u32 upper = value >> hash_size;
    result = mask & (value ^ upper);
  }

  return result;
}

u32 Hash64::fold32(u8 hash_size)
{
  assert(hash_size <= 32);
  u32 mask = (1u << hash_size) - 1;
  u32 upper = value >> hash_size;
  u32 result = mask & (value ^ upper);
  return result;
}

u64 Hash64::fold64(u8 hash_size)
{
  assert(32 < hash_size && hash_size <= 64);

  u64 result;

  if (hash_size == 64)
  {
    result = value;
  }
  else
  {
    u64 mask = (1ull << hash_size) - 1;
    u64 upper = value >> hash_size;
    result = mask & (value ^ upper);
  }

  return result;
}
