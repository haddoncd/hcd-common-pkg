#ifdef StackAllocator_hpp
#error Multiple inclusion
#endif
#define StackAllocator_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef allocator_hpp
#error "Please include allocator.hpp before this file"
#endif


struct StackAllocator
{
  u8 *base;
  u8 *firstByteBeyond;
  u8 *firstFree;

  void init(u8 *memory, size_t size);
  void clear();
};

u8 *allocUninitRaw(StackAllocator *stack, size_t size, size_t alignment = 1);
u8 *allocRaw(StackAllocator *stack, size_t size, size_t alignment = 1);
