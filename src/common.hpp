#ifdef common_hpp
#error Multiple inclusion
#endif
#define common_hpp

#include <limits>
#include <utility>
#include <type_traits>

#include <cstdint>
#include <cassert>
#include <cstring>
#include <cstdio>
#include <cstdarg>

#include <malloc.h>


typedef uint8_t   b8;
typedef uint16_t  b16;
typedef uint32_t  b32;
typedef uint64_t  b64;
typedef uintptr_t bptr;

typedef int8_t    i8;
typedef int16_t   i16;
typedef int32_t   i32;
typedef int64_t   i64;
typedef intptr_t  iptr;

typedef uint8_t   u8;
typedef uint16_t  u16;
typedef uint32_t  u32;
typedef uint64_t  u64;
typedef uintptr_t uptr;

typedef float     f32;
#define F32_MAX std::numeric_limits<f32>::max()
static_assert(std::numeric_limits<f32>::has_infinity, "Cannot define F32_INF");
#define F32_INF std::numeric_limits<f32>::infinity()
static_assert(std::numeric_limits<f32>::has_quiet_NaN, "Cannot define F32_NAN");
#define F32_NAN std::numeric_limits<f32>::quiet_NaN()

typedef double    f64;
#define F64_MAX std::numeric_limits<f64>::max()
static_assert(std::numeric_limits<f64>::has_infinity, "Cannot define F64_INF");
#define F64_INF std::numeric_limits<f64>::infinity()
static_assert(std::numeric_limits<f64>::has_quiet_NaN, "Cannot define F64_NAN");
#define F64_NAN std::numeric_limits<f64>::quiet_NaN()

#define C_STRING_INDIRECT(a) #a
#define C_STRING(a) C_STRING_INDIRECT(a)

template <size_t L>
using StringLiteral = const char (&)[L];

template <typename ...Ts>
constexpr bool TypesAreAllStringLiteral = false;

template <size_t ...Ls>
constexpr bool TypesAreAllStringLiteral<StringLiteral<Ls>...> = true;

#define SI_KILO          1000LL
#define SI_MEGA (SI_KILO*1000LL)
#define SI_GIGA (SI_MEGA*1000LL)
#define SI_TERA (SI_GIGA*1000LL)

#define SI_KIBI          1024LL
#define SI_MEBI (SI_KIBI*1024LL)
#define SI_GIBI (SI_MEBI*1024LL)
#define SI_TEBI (SI_GIBI*1024LL)

#define arrayCount(a) (sizeof(a) / sizeof((a)[0]))

template <typename T>
void zeroStruct(T *t)
{
  memset(t, 0, sizeof(T));
}

template <typename T>
void zeroArray(T *array, size_t count)
{
  memset(array, 0, count * sizeof(T));
}

template <typename T, size_t N>
void zeroArray(T (&array)[N])
{
  zeroArray(array, N);
}

template <typename T>
constexpr T minimum(T a, T b)
{
  return a < b ? a : b;
}

template <typename T>
constexpr T maximum(T a, T b)
{
  return a < b ? b : a;
}

template <typename T>
constexpr T clamp(T a, T b, T c)
{
  assert(b <= c);
  return a < b ? b : c < a ? c : a;
}

template <typename T>
constexpr T square(T a)
{
  return a * a;
}

template <typename T>
constexpr T invSqrt(T a)
{
  // TODO: intrinsics?
  return ((T) 1) / sqrt(a);
}

template <typename T>
constexpr i32 compare(T a, T b)
{
  return (b < a) - (a < b);
}

#define signum(a) compare(a, 0)

template <typename T>
void swap(T &a, T &b)
{
  T tmp = a;
  a = b;
  b = a;
}

template <typename T>
constexpr T absDiff(T a, T b)
{
  return (a < b) ? b - a : a - b;
}

template <typename T>
constexpr bool isPowerOfTwo(T a)
{
  return ((0 < a) && ((a & (~a + 1)) == a));
}

// Stupid hack to work around limitations in C++11 constexpr
constexpr u32 ceilPowerOfTwo_u32_internal16(u32 a)
{
  return a | a >> 16;
}

constexpr u32 ceilPowerOfTwo_u32_internal8(u32 a)
{
  return ceilPowerOfTwo_u32_internal16(a | a >> 8);
}

constexpr u32 ceilPowerOfTwo_u32_internal4(u32 a)
{
  return ceilPowerOfTwo_u32_internal8(a | a >> 4);
}

constexpr u32 ceilPowerOfTwo_u32_internal2(u32 a)
{
  return ceilPowerOfTwo_u32_internal4(a | a >> 2);
}

constexpr u32 ceilPowerOfTwo_u32_internal1(u32 a)
{
  return ceilPowerOfTwo_u32_internal2(a | a >> 1);
}

constexpr u32 ceilPowerOfTwo(u32 a)
{
  return a == 0 ? 0 : ceilPowerOfTwo_u32_internal1(a - 1) + 1;
}

constexpr u64 ceilPowerOfTwo_u64_internal32(u64 a)
{
  return a | a >> 32;
}

constexpr u64 ceilPowerOfTwo_u64_internal16(u64 a)
{
  return ceilPowerOfTwo_u64_internal32(a | a >> 16);
}

constexpr u64 ceilPowerOfTwo_u64_internal8(u64 a)
{
  return ceilPowerOfTwo_u64_internal16(a | a >> 8);
}

constexpr u64 ceilPowerOfTwo_u64_internal4(u64 a)
{
  return ceilPowerOfTwo_u64_internal8(a | a >> 4);
}

constexpr u64 ceilPowerOfTwo_u64_internal2(u64 a)
{
  return ceilPowerOfTwo_u64_internal4(a | a >> 2);
}

constexpr u64 ceilPowerOfTwo_u64_internal1(u64 a)
{
  return ceilPowerOfTwo_u64_internal2(a | a >> 1);
}

constexpr u64 ceilPowerOfTwo(u64 a)
{
  return a == 0 ? 0 : ceilPowerOfTwo_u64_internal1(a - 1) + 1;
}

constexpr u8 u32Log2_internal1(u32 a)
{
  return (a > ((1u << 1) - 1))
    ? 1
    : 0;
}

constexpr u8 u32Log2_internal2(u32 a)
{
  return (a > ((1u << 2) - 1))
    ? u32Log2_internal1(a >> 2) + 2
    : u32Log2_internal1(a);
}

constexpr u8 u32Log2_internal4(u32 a)
{
  return (a > ((1u << 4) - 1))
    ? u32Log2_internal2(a >> 4) + 4
    : u32Log2_internal2(a);
}

constexpr u8 u32Log2_internal8(u32 a)
{
  return (a > ((1u << 8) - 1))
    ? u32Log2_internal4(a >> 8) + 8
    : u32Log2_internal4(a);
}

constexpr u8 u32Log2_internal16(u32 a)
{
  return (a > ((1u << 16) - 1))
    ? u32Log2_internal8(a >> 16) + 16
    : u32Log2_internal8(a);
}

constexpr u8 u32Log2(u32 a)
{
  return (a == 0) ? UINT8_MAX : u32Log2_internal16(a);
}

template <typename T2, typename T1>
void assertRepresentableAs(T1 x);

template <typename T2, typename T1>
T2 castOrAssert(T1 x)
{
  assertRepresentableAs<T2>(x);
  return (T2) x;
}

constexpr uptr align(uptr address, uptr alignment)
{
  return address + (address % alignment ? alignment - (address % alignment) : 0);
}

template <typename T>
constexpr T *align(T *address, uptr alignment = alignof(T))
{
  return (T *)align((uptr) address, alignment);
}

#define memberType(t, e) decltype(((t *)0)->e)
#define memberSize(t, e) sizeof(memberType(t, e))
#define offsetOf(t, e) ((size_t)&(((t *)0)->e))

#define  PI_32 3.141592653589793238462643383279502884f
#define  PI_64 3.141592653589793238462643383279502884
#define TAU_32 6.283185307179586476925286766559005768f
#define TAU_64 6.283185307179586476925286766559005768

#define BREAKABLE_START do
#define BREAKABLE_END   while(false);

#define boolXor(a, b) (((a) != 0) != ((b) != 0))

#define NO_COPY_OR_MOVE(T)             \
  T(T const &) = delete;               \
  void operator=(T const &t) = delete; \
  T(T &&) = delete;

#define DECLARE_ENUM_PREINCREMENT Enum& operator++(Enum& value);

#define DEFINE_ENUM_PREINCREMENT \
  Enum& operator++(Enum& value)  \
  {                              \
    value = (Enum) (value + 1);  \
    return value;                \
  }

#define forEachEnum(value, enumType) for (auto value = (enumType::Enum) 0; value < enumType::ENUM_COUNT; ++value)

// FIXME: Win32 specific?

#define variableArray(T, count) ((T *) _alloca(sizeof(T) * (count)))
