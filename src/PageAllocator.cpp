#ifdef PageAllocator_cpp
#error Multiple inclusion
#endif
#define PageAllocator_cpp

#ifndef PageAllocator_hpp
#include "PageAllocator.hpp"
#endif

size_t PageAllocator::Object::sizeWith(size_t size)
{
  return offsetOf(PageAllocator::Object, memory[size]);
}

void PageAllocator::State::init(HeapAllocator *heap)
{
  this->heap    = heap;
  page          = allocUninit<PageAllocator::Page>(heap);
  page->next    = 0;
  page->objects = 0;
  pageNum       = 0;
  freePages     = 0;
  freePageCount = 0;
}

void PageAllocator::State::destroy()
{
  while (page)
  {
    while (page->objects)
    {
      PageAllocator::Object *o = page->objects;
      page->objects = o->next;
      heap->free(o);
    }

    PageAllocator::Page *p = page;
    page = p->next;
    heap->free(p);
  }

  while (freePages)
  {
    PageAllocator::Page *p = freePages;
    freePages = p->next;
    heap->free(p);
  }

  *this = {};
}

PageAllocator PageAllocator::reuseState(PageAllocator::State *state)
{
  PageAllocator result;
  result.state    = state;
  result.pageNum  = 0;
  result.position = 0;
  return result;
}

void PageAllocator::init(HeapAllocator *heap)
{
  state = allocUninit<PageAllocator::State>(heap);
  state->init(heap);
  pageNum  = 0;
  position = 0;
}

void PageAllocator::destroy()
{
  HeapAllocator *heap = state->heap;
  state->destroy();
  heap->free(state);
  *this = {};
}

u8 *PageAllocator::allocInternal(bool zero, size_t size, size_t alignment)
{
  assert(pageNum <= state->pageNum);
  while (pageNum < state->pageNum)
  {
    // pop pages from state
    // NB. pageNums are unsigned, so we can never pop page 0.

    PageAllocator::Page *old_page = state->page;
    state->page = old_page->next;
    --state->pageNum;
    assert(state->page);

    while (old_page->objects)
    {
      PageAllocator::Object *old_object = old_page->objects;
      old_page->objects = old_object->next;
      state->heap->free(old_object);
    }

    if (state->freePageCount < PageAllocator::MAX_FREE_PAGES)
    {
      old_page->next = state->freePages;
      state->freePages = old_page;
      ++state->freePageCount;
    }
    else
    {
      state->heap->free(old_page);
    }
  }

  while (state->page->objects &&
          (position <= state->page->objects->position))
  {
    // pop objects from page

    PageAllocator::Object *old_object = state->page->objects;
    state->page->objects = old_object->next;
    state->heap->free(old_object);
  }

  u8 *result;

  if (size < PageAllocator::Object::MIN_SIZE)
  {
    u32 aligned_position = align(position + (uptr)state->page->memory,
                                 alignment) -
                           (uptr)state->page->memory;
    if (PageAllocator::Page::CAPACITY < aligned_position + size)
    {
      // push another page onto the state

      PageAllocator::Page *new_page;
      if (state->freePageCount)
      {
        assert(state->freePages);
        new_page = state->freePages;
        state->freePages = new_page->next;
        --state->freePageCount;
      }
      else
      {
        new_page = allocUninit<PageAllocator::Page>(state->heap);
        new_page->objects = 0;
      }

      new_page->next = state->page;
      state->page    = new_page;
      ++state->pageNum;

      ++pageNum;
      position = 0;

      aligned_position = align(position + (uptr)state->page->memory,
                      alignment) -
                 (uptr)state->page->memory;
    }

    result = &state->page->memory[aligned_position];
    position = aligned_position + castOrAssert<u32>(size);
  }
  else
  {
    // allocate as a single object

    size_t object_size = PageAllocator::Object::sizeWith(size);
    PageAllocator::Object *new_object = zero ?
      allocSized<PageAllocator::Object>(state->heap, object_size) :
      allocUninitSized<PageAllocator::Object>(state->heap, object_size);
    zero = false;

    new_object->position = position;
    ++position;

    new_object->next = state->page->objects;
    state->page->objects = new_object;
    result = new_object->memory;
  }

  if (zero)
  {
    zeroArray(result, size);
  }

  return result;
}

u8 *allocUninitRaw(PageAllocator *allocator, size_t size, size_t alignment)
{
  u8 *result = allocator->allocInternal(false, size, alignment);
  return result;
}

u8 *allocRaw(PageAllocator *allocator, size_t size, size_t alignment)
{
  u8 *result = allocator->allocInternal(true, size, alignment);
  return result;
}

void PageAllocator::clear()
{
  pageNum  = 0;
  position = 0;
}
