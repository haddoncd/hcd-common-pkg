#ifdef soa_hpp
#error Multiple inclusion
#endif
#define soa_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

namespace soa
{
  template <typename T>
  constexpr size_t addSize(size_t current_size, u64 count)
  {
    return align(current_size, alignof(T)) + (count * sizeof(T));
  }

  template <typename T1, typename T2, typename... Ts>
  constexpr size_t addSize(size_t current_size, u64 count)
  {
    return addSize<T2, Ts...>(addSize<T1>(current_size, count), count);
  }

  template <typename... Ts>
  constexpr size_t bufSizeWith(u64 count)
  {
    return addSize<Ts...>(0, count);
  }

  template <typename T>
  void *setIndexPtr(void **index, void *buf, u64 count)
  {
    T *array = align((T *)buf);
    *index = array;
    return &array[count];
  }

  template <typename T1, typename T2, typename... Ts>
  void *setIndexPtr(void **index, void *buf, u64 count)
  {
    void *remaining_buf = setIndexPtr<T1>(index, buf, count);
    return setIndexPtr<T2, Ts...>(++index, remaining_buf, count);
  }

  template <typename... Ts>
  void initIndex(void **index, void *buf, u64 count)
  {
    setIndexPtr<Ts...>(index, buf, count);
  }
}
