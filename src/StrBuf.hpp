#ifdef StrBuf_hpp
#error Multiple inclusion
#endif
#define StrBuf_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

namespace StrBuf
{
  template <u32 N>
  struct Fixed
  {
    u32   length;
    char  data[N];

    Str str() const
    {
      return Str{length, data};
    }

    ZStr zStr()
    {
      bool ok = nullTerminate();
      assert(ok);
      return ZStr{length, data};
    }

    void clear()
    {
      length = 0;
    }

    bool nullTerminate()
    {
      return Generic::nullTerminate((Fixed<0> *) this, N);
    }

    template <typename ...Ts>
    bool append(Ts &&... args)
    {
      return Generic::append((Fixed<0> *) this, N, std::forward<Ts>(args)...);
    }

    template <typename ...Ts>
    bool overwrite(Ts &&... args)
    {
      clear();
      return append(std::forward<Ts>(args)...);
    }

    void replaceAll(char find, char replace)
    {
      Generic::replaceAll((Fixed<0> *) this, find, replace);
    }
  };

  namespace Generic
  {
    bool nullTerminate(Fixed<0> *buf, u32 capacity);

    bool append(Fixed<0> *buf, u32 capacity, char c);

    bool append(Fixed<0> *buf, u32 capacity, Str str);

    template <size_t L>
    bool append(Fixed<0> *buf, u32 capacity, StringLiteral<L> string)
    {
      return append(buf, capacity, Str::fromLiteral(string));
    }

    template <typename T1, typename ...Ts>
    bool append(Fixed<0> *buf, u32 capacity, const char *fmt, const T1 &arg1, const Ts &... args)
    {
      assert(buf->length <= capacity);
      u32 spare_capacity = capacity - buf->length;
      u32 print_length = snprintf(buf->data + buf->length, spare_capacity, fmt, arg1, args...);
      bool result = (print_length < spare_capacity);
      buf->length += result ? print_length
                            : spare_capacity - 1;
      return result;
    }

    void replaceAll(Fixed<0> *buf, char find, char replace);
  }

  struct Variable
  {
    u32       capacity;
    union
    {
      Fixed<0> generic;
      struct
      {
        u32   length;
        char  data[];
      };
    };

    static size_t sizeWith(u32 capacity);

    NO_COPY_OR_MOVE(Variable)

    void setSize(u32 capacity);

    Str str() const;

    ZStr zStr();

    void clear();

    bool nullTerminate();

    template <typename ...Ts>
    bool append(Ts &&... args)
    {
      return Generic::append(&generic, capacity, std::forward<Ts>(args)...);
    }

    template <typename ...Ts>
    bool overwrite(Ts &&... args)
    {
      clear();
      return append(std::forward<Ts>(args)...);
    }

    void replaceAll(char find, char replace);
  };

  template <typename T1, typename ...Ts>
  u32 printfLength(const char *fmt, const T1 &arg1, const Ts &... args)
  {
    int result = snprintf(0, 0, fmt, arg1, args...);
    assert(0 <= result);
    return result + 1; // null terminator
  }

  struct Null
  {
    u32 size;
    bool nullTerminated;

    Null();

    void clear();

    bool nullTerminate();

    void undoNullTerminate();

    bool append(char c);

    template <size_t L>
    bool append(StringLiteral<L> string)
    {
      return append(Str::fromLiteral(string));
    }

    bool append(Str str);

    template <typename T1, typename ...Ts>
    bool append(const char *fmt, const T1 &arg1, const Ts &... args)
    {
      undoNullTerminate();
      size += printfLength(fmt, arg1, args...);
      return true;
    }

    template <typename ...Ts>
    bool overwrite(Ts &&... args)
    {
      clear();
      return append(std::forward<Ts>(args)...);
    }
  };
}

#define StrBuf_Variable_localAllocAndInit(p, n)                           \
do                                                                        \
{                                                                         \
  u32 capacity = n;                                                       \
  p = (StrBuf::Variable *) _alloca(StrBuf::Variable::sizeWith(capacity)); \
  p->capacity = capacity;                                                 \
  p->length = 0;                                                          \
}                                                                         \
while (false)

