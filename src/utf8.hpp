#ifdef utf8_hpp
#error Multiple inclusion
#endif
#define utf8_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif


struct utf8_Codepoint
{
  u8  bytes[4];
  u32 byteCount;
};

utf8_Codepoint utf8_codepoint(u32 codepoint);
