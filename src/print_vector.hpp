#ifdef print_vector_hpp
#error Multiple inclusion
#endif
#define print_vector_hpp

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif



template <typename S, typename T>
bool print(S out, V2<T> v)
{
  return print(out, "("_s, fmt(v.x), ", "_s, fmt(v.y), ")"_s);
}

template <typename S, typename T>
bool print(S out, V3<T> v)
{
  return print(out, "("_s, fmt(v.x), ", "_s, fmt(v.y), ", "_s, fmt(v.z), ")"_s);
}

template <typename S, typename T>
bool print(S out, V4<T> v)
{
  return print(out, "("_s, fmt(v.x), ", "_s, fmt(v.y), ", "_s, fmt(v.z), ", "_s, fmt(v.w), ")"_s);
}
