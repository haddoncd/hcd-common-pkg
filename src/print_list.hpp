#ifdef print_list_hpp
#error Multiple inclusion
#endif
#define print_list_hpp

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

#ifndef List_hpp
#error "Please include List.hpp before this file"
#endif



template <typename S, typename T>
bool print(S out, List<T> *list)
{
  bool result = print(out, "{ "_s);
  for (auto *link = list->first();
       result && link != list->sentinel();
       link = link->next)
  {
    if (link != list->first()) print(out, ", "_s);
    result &= print(out, fmt(link), " -> "_s, link->data);
  }
  result &= print(out, " }"_s);
  return result;
}

template <typename S, typename T>
bool print(S out, T *link)
{
  return print(out, fmt(link), " -> { prev: "_s, fmt(link->prev), ", next: "_s, fmt(link->next), ", data: "_s, link->data, " }"_s);
}
