#ifdef allocator_hpp
#error Multiple inclusion
#endif
#define allocator_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif


template <typename T, typename A>
T *alloc(A al, size_t alignment = sizeof(T))
{
  return (T *)allocRaw(al, sizeof(T), alignment);
}

template <typename T, typename A>
T *allocUninit(A al, size_t alignment = sizeof(T))
{
  return (T *)allocUninitRaw(al, sizeof(T), alignment);
}

template <typename T, typename A>
T *allocArray(A al, size_t count, size_t alignment = sizeof(T))
{
  return (T *)allocRaw(al, count * sizeof(T), alignment);
}

template <typename T, typename A>
T *allocUninitArray(A al, size_t count, size_t alignment = sizeof(T))
{
  return (T *)allocUninitRaw(al, count * sizeof(T), alignment);
}

template <typename T, typename A>
T *allocSized(A al, size_t size, size_t alignment = sizeof(T))
{
  assert(sizeof(T) <= size);
  return (T *)allocRaw(al, size, alignment);
}

template <typename T, typename A>
T *allocUninitSized(A al, size_t size, size_t alignment = sizeof(T))
{
  assert(sizeof(T) <= size);
  return (T *)allocUninitRaw(al, size, alignment);
}

template <typename T, typename A, typename... Us>
T *allocSizedStruct(A al, Us &&... args)
{
  T *result = allocSized<T>(al, T::sizeWith(std::forward<Us>(args)...));
  return result;
}

template <typename T, typename A, typename... Us>
T *allocAlignedSizedStruct(A al, size_t alignment, Us &&... args)
{
  T *result = allocSized<T>(al, T::sizeWith(std::forward<Us>(args)...), alignment);
  return result;
}

template <typename T, typename A, typename... Us>
T *allocUninitSizedStruct(A al, Us &&... args)
{
  T *result = allocUninitSized<T>(al, T::sizeWith(std::forward<Us>(args)...));
  return result;
}

template <typename T, typename A, typename... Us>
T *allocUninitAlignedSizedStruct(A al, size_t alignment, Us &&... args)
{
  T *result = allocUninitSized<T>(al, T::sizeWith(std::forward<Us>(args)...), alignment);
  return result;
}

template <typename T, typename A, typename... Us>
T *allocWith(A al, Us &&... args)
{
  T *result = allocSizedStruct<T>(al, std::forward<Us>(args)...);
  result->setSize(std::forward<Us>(args)...);
  return result;
}

template <typename T, typename A, typename... Us>
T *allocAlignedWith(A al, size_t alignment, Us &&... args)
{
  T *result = allocAlignedSizedStruct<T>(al, alignment, std::forward<Us>(args)...);
  result->setSize(std::forward<Us>(args)...);
  return result;
}

template <typename T, typename A, typename... Us>
T *allocUninitWith(A al, Us &&... args)
{
  T *result = allocUninitSizedStruct<T>(al, std::forward<Us>(args)...);
  result->setSize(std::forward<Us>(args)...);
  return result;
}

template <typename T, typename A, typename... Us>
T *allocUninitAlignedWith(A al, size_t alignment, Us &&... args)
{
  T *result = allocUninitAlignedSizedStruct<T>(al, alignment, std::forward<Us>(args)...);
  result->setSize(std::forward<Us>(args)...);
  return result;
}
