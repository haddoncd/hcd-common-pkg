#ifdef HeapAllocator_hpp
#error Multiple inclusion
#endif
#define HeapAllocator_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef allocator_hpp
#error "Please include allocator.hpp before this file"
#endif

struct HeapAllocator
{
  void *(*malloc)(size_t);
  void *(*calloc)(size_t, size_t);
  void *(*realloc)(void *, size_t);
  void (*free)(void *);
};

extern HeapAllocator *STD_HEAP;

u8 *allocRaw(HeapAllocator *heap, size_t size, size_t ignored = 0);
u8 *allocUninitRaw(HeapAllocator *heap, size_t size, size_t ignored = 0);
