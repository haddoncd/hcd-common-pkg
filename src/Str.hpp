#ifdef Str_hpp
#error Multiple inclusion
#endif
#define Str_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif


struct Str
{
  u64         length;
  const char *data;

  static constexpr Str empty()
  {
    return Str{0, 0};
  }

  template <size_t L>
  static constexpr Str fromLiteral(StringLiteral<L> string)
  {
    return Str{L - 1, string};
  }

  Str slice(u64 start, u64 length) const;

  Str left(u64 length) const;

  Str trimLeft(u64 length) const;

  Str right(u64 length) const;

  Str trimRight(u64 length) const;

  bool operator==(const Str &that) const;

  bool operator!=(const Str &that) const;

  bool beginsWith(Str prefix) const;

  bool endsWith(Str suffix) const;

  static constexpr u64 NOT_FOUND = UINT64_MAX;

  u64 find(Str needle) const;

  template <size_t L1>
  u64 find(StringLiteral<L1> needle) const
  {
    return find(Str::fromLiteral(needle));
  }

  u64 findReverse(Str needle) const;

  template <size_t L1>
  u64 findReverse(StringLiteral<L1> needle) const
  {
    return findReverse(Str::fromLiteral(needle));
  }

  template <typename ...Ts>
  u64 findNot(char needle_1, Ts... others) const
  {
    constexpr size_t needle_count = 1 + sizeof...(Ts);
    char needles[needle_count] = { needle_1, others... };

    u64 result = Str::NOT_FOUND;

    for (u64 i = 0; i < length; ++i)
    {
      bool match = false;

      for (u64 j = 0; j < needle_count; ++j)
      {
        if (data[i] == needles[j])
        {
          match = true;
          break;
        }
      }

      if (!match)
      {
        result = i;
        break;
      }
    }
    return result;
  }

  template <typename ...Ts>
  u64 findNotReverse(char needle_1, Ts... others) const
  {
    constexpr size_t needle_count = 1 + sizeof...(Ts);
    char needles[needle_count] = { needle_1, others... };

    u64 result = Str::NOT_FOUND;

    for (u64 i = length - 1; i < length; --i)
    {
      bool match = false;

      for (u64 j = 0; j < needle_count; ++j)
      {
        if (data[i] == needles[j])
        {
          match = true;
          break;
        }
      }

      if (!match)
      {
        result = i;
        break;
      }
    }
    return result;
  }

  struct MultiFindResult
  {
    u64 location;
    u64 needle;

    bool found() { return location != NOT_FOUND; }
  };

  template <size_t N>
  MultiFindResult find(const Str (&needles)[N]) const
  {
    MultiFindResult result = { NOT_FOUND, NOT_FOUND };

    for (u64 i = 0; i < length; ++i)
    {
      Str remaining = trimLeft(i);

      for (u64 j = 0; j < N; ++j)
      {
        if (remaining.beginsWith(needles[j]))
        {
          result.location = i;
          result.needle   = j;
          goto end;
        }
      }
    }
    end:
    return result;
  }

  template <typename ...Ts>
  MultiFindResult find(Str needle_1, Str needle_2, const Ts &... others) const
  {
    Str needles[] = { needle_1, needle_2, others... };
    return find(needles);
  }

  template <size_t L1, size_t L2, typename ...Ts>
  MultiFindResult find(StringLiteral<L1> needle_1, StringLiteral<L2> needle_2, const Ts &... others) const
  {
    return find(Str::fromLiteral(needle_1), Str::fromLiteral(needle_2), Str::fromLiteral(others)...);
  }

  template <typename ...Ts>
  MultiFindResult findReverse(Str needle_1, Str needle_2, const Ts &... others) const
  {
    Str needles[] = { needle_1, needle_2, others... };

    MultiFindResult result = { NOT_FOUND, NOT_FOUND };

    for (u64 i = 0; i < length; ++i)
    {
      Str remaining = trimRight(i);

      for (u64 j = 0; j < arrayCount(needles); ++j)
      {
        if (remaining.endsWith(needles[j]))
        {
          result.location = length - i - needles[j].length;
          result.needle   = j;
          goto end;
        }
      }
    }
    end:
    return result;
  }

  template <size_t L1, size_t L2, typename ...Ts>
  MultiFindResult findReverse(StringLiteral<L1> needle_1, StringLiteral<L2> needle_2, const Ts &... others) const
  {
    return findReverse(Str::fromLiteral(needle_1), Str::fromLiteral(needle_2), Str::fromLiteral(others)...);
  }

  u64 count(char needle) const;

  Str chop(Str delim);

  template <typename ...Ts>
  Str chop(Str delim_1, Str delim_2, Ts... others)
  {
    Str result;

    constexpr size_t delims_count = 2 + sizeof...(Ts);
    Str delims[delims_count] = { delim_1, delim_2, others... };

    Str::MultiFindResult find_result = find(delims);

    if (find_result.found())
    {
      result = Str{find_result.location, data};
      u64 find_result_end = find_result.location +
                      delims[find_result.needle].length;
      length -= find_result_end;
      data   += find_result_end;
    }
    else
    {
      result = *this;
      *this = Str::empty();
    }

    return result;
  }

  template <size_t L1, typename ...Ts>
  Str chop(StringLiteral<L1> needle_1, const Ts &... others)
  {
    return chop(Str::fromLiteral(needle_1), Str::fromLiteral(others)...);
  }

  template <typename ...Ts>
  Str removeLeading(char to_remove_1, Ts... others) const
  {
    u64 find_result = findNot(to_remove_1, others...);
    return find_result == Str::NOT_FOUND ? Str::empty() :
      Str{length - find_result,
          data   + find_result};
  }

  template <typename ...Ts>
  Str removeTrailing(char to_remove_1, Ts... others) const
  {
    u64 find_result = findNotReverse(to_remove_1, others...);
    return find_result == Str::NOT_FOUND ? Str::empty() :
      Str{find_result + 1, data};
  }

  Str truncatedAtFirst(Str delim) const;

  template <size_t L1>
  Str truncatedAtFirst(StringLiteral<L1> delim) const
  {
    return truncatedAtFirst(Str::fromLiteral(delim));
  }

  template <typename ...Ts>
  u64 truncatedAtFirst(Str delim_1, Str delim_2, Ts... others) const
  {
    return Str{minimum(length, find(delim_1, delim_2, others).location),
               data};
  }

  template <size_t L1, size_t L2, typename ...Ts>
  u64 truncatedAtFirst(StringLiteral<L1> delim_1, StringLiteral<L2> delim_2, const Ts &... others) const
  {
    return truncatedAtFirst(Str::fromLiteral(delim_1), Str::fromLiteral(delim_2), Str::fromLiteral(others)...);
  }

  Str truncatedAtLast(Str delim) const;

  template <size_t L1>
  Str truncatedAtLast(StringLiteral<L1> delim) const
  {
    return truncatedAtLast(Str::fromLiteral(delim));
  }

  template <typename ...Ts>
  u64 truncatedAtLast(Str delim_1, Str delim_2, Ts... others) const
  {
    return Str{minimum(length, findReverse(delim_1, delim_2, others).location),
               data};
  }

  template <size_t L1, size_t L2, typename ...Ts>
  u64 truncatedAtLast(StringLiteral<L1> delim_1, StringLiteral<L2> delim_2, const Ts &... others) const
  {
    return truncatedAtLast(Str::fromLiteral(delim_1), Str::fromLiteral(delim_2), Str::fromLiteral(others)...);
  }

  f64 parseF64() const;
};

#define STR(a) ZSTR(a)

i32 compare(Str a, Str b);

struct ZStr
{
  Str s;

  static constexpr const char empty_null_terminator = 0;
  static constexpr ZStr empty();

  template <size_t L>
  static constexpr ZStr fromLiteral(StringLiteral<L> string)
  {
    return ZStr{{L - 1, string}};
  }

  operator Str() const;
  operator char *() const;
  operator const char *() const;

  bool operator==(const Str &that) const;
  bool operator!=(const Str &that) const;

  f64 parseF64() const;
};

constexpr ZStr operator"" _s(const char *data, size_t length);

#define ZSTR(a) C_STRING(a)_s

ZStr zStr(const char *data);

#define ZStr_localAllocCopy(zstr, str)             \
do                                                 \
{                                                  \
  Str src = (str);                                 \
  char *buf = variableArray(char, src.length + 1); \
  memcpy(buf, src.data, src.length);               \
  buf[src.length] = 0;                             \
  (zstr) = ZStr{{src.length, buf}};                \
}                                                  \
while (false)
