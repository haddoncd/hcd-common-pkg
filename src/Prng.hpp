#ifdef Prng_hpp
#error Multiple inclusion
#endif
#define Prng_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif



struct Prng
{
  // These state variables must be initialized so that they are not all zero.
  u32 x, y, z, w;

  // Uniform random raw u32 (ie. in [0, UINT32_MAX])
  u32 next();

  u32 nextU32();
  u32 nextU32(u32 max);
  u32 nextU32(u32 a, u32 b);
  i32 nextI32(i32 a, i32 b);

  // Uniform random f32 in [a, b]
  // default args intended to allow half specification:
  //   nextF32(7.0f) => in [0.0f, 7.0f]
  f32 nextF32(f32 a = 1.0f, f32 b = 0.0f);

  // Random boolean with P(true) = p
  bool nextBool(f32 p = 0.5f);
};
