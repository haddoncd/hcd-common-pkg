#ifdef StrBuf_cpp
#error Multiple inclusion
#endif
#define StrBuf_cpp

#ifndef StrBuf_hpp
#include "StrBuf.hpp"
#endif


namespace StrBuf
{
  namespace Generic
  {
    bool nullTerminate(Fixed<0> *buf, u32 capacity)
    {
      bool result = buf->length < capacity;
      if (result)
      {
        buf->data[buf->length] = 0;
      }
      return result;
    }

    bool append(Fixed<0> *buf, u32 capacity, char c)
    {
      u32 spare_capacity = capacity - buf->length;
      bool result = 1 <= spare_capacity;
      if (result) buf->data[buf->length++] = c;
      return result;
    }

    bool append(Fixed<0> *buf, u32 capacity, Str str)
    {
      u32 spare_capacity = capacity - buf->length;
      bool result = str.length <= spare_capacity;
      memcpy(buf->data + buf->length,
             str.data,
             result ? str.length : spare_capacity);
      buf->length = result ? buf->length + str.length : capacity;
      return result;
    }

    void replaceAll(Fixed<0> *buf, char find, char replace)
    {
      for (u32 i = 0; i < buf->length; ++i)
      {
        if (buf->data[i] == find)
        {
          buf->data[i] = replace;
        }
      }
    }
  }

  size_t Variable::sizeWith(u32 capacity)
  {
    return offsetOf(Variable, data[capacity]);
  }

  void Variable::setSize(u32 capacity)
  {
    this->capacity = capacity;
  }

  Str Variable::str() const
  {
    return Str{length, data};
  }

  ZStr Variable::zStr()
  {
    bool ok = nullTerminate();
    assert(ok);
    return ZStr{length, data};
  }

  void Variable::clear()
  {
    length = 0;
  }

  bool Variable::nullTerminate()
  {
    return Generic::nullTerminate(&generic, capacity);
  }

  void Variable::replaceAll(char find, char replace)
  {
    return Generic::replaceAll(&generic, find, replace);
  }

  Null::Null()
  {
    clear();
  }

  void Null::clear()
  {
    size = 0;
    nullTerminated = false;
  }

  bool Null::nullTerminate()
  {
    if (!nullTerminated)
    {
      u32 old_size = size;
      ++size;
      assert(old_size <= size);
      nullTerminated = true;
    }
    return true;
  }

  void Null::undoNullTerminate()
  {
    if (nullTerminated)
    {
      assert(size);
      --size;
      nullTerminated = false;
    }
  }

  bool Null::append(char c)
  {
    undoNullTerminate();
    u32 old_size = size;
    ++size;
    assert(old_size <= size);
    return true;
  }

  bool Null::append(Str str)
  {
    undoNullTerminate();
    u32 old_size = size;
    size += str.length;
    assert(old_size <= size);
    return true;
  }
}
