#ifdef Str_Hash_hpp
#error Multiple inclusion
#endif
#define Str_Hash_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Hash_hpp
#error "Please include Hash.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

template<>
Hash32 Hash32::hash(Str str) const
{
  return hash((u8 *)str.data, (u32) minimum(str.length, (u64)UINT32_MAX));
}

template<>
Hash64 Hash64::hash(Str str) const
{
  return hash((u8 *)str.data, (u32) minimum(str.length, (u64)UINT32_MAX));
}
