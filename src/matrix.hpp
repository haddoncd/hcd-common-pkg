#ifdef matrix_hpp
#error Multiple inclusion
#endif
#define matrix_hpp

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif


// Matrices as defined here are column-major in memory,
// however, "constructors" use row-major for convenience, ie:
//                ┌        ┐
//   m2(x0, x1,   │ x0  x1 │
//      y0, y1) = │ y0  y1 │ = { x0, y0, x1, y1 }
//                └        ┘

// Matrix dimensions are columns x rows, ie:
//   M3x2 a * M4x3 b = M4x2 c
//
// Oblong matrices are called (eg) Mx<2,3,T> or M2x3<T>
// Square types are called (eg) Mx<2,2,T>, M<2,T> or simply M2<T>

// Multiplication with vectors is only allowed with the vector on the left,
// and treats vectors as columns, ie. V4 => M1x4

// NB. Due to the "backwards" handedness of our matrix multiplication,
// operator *= is also backwards to cancel this, ie:
//
// For vectors:
//     b *= A
//   means:
//     b' = A * b
//   resulting in b having been "transformed by" A
//
// For matrices:
//     B *= A
//   means:
//     B' = A * B
//   resulting in B' "transforming by" B "after" A, when multiplying a vector:
//     B' * v = B * A * v.



////////////////////////////////
// Meta-template wrapper type //
////////////////////////////////

template <u32 N, u32 M, typename T> struct Mx;

template <u32 N, typename T>
using M = Mx<N,N,T>;


///////////
// 2 x 2 //
///////////

template <typename T>
using M2 = M<2,T>;

template<typename T>
struct Mx<2,2,T>
{
  union
  {
    struct
    {
      T x0, y0,
        x1, y1;
    };
    T     v[4];
    V2<T> cols[2];
  };

  static constexpr M2<T> identity()
  {
    return m2<T>(1.0f, 0.0f,
                 0.0f, 1.0f);
  }

  V2<T> row(u32 i) const
  {
    return v2(cols[0].v[i], cols[1].v[i]);
  }

  M2<T> transposed()
  {
    M2<T> result;

    result.x0 = x0;
    result.y0 = x1;
    result.x1 = y0;
    result.y1 = y1;

    return result;
  }

  void transpose()
  {
    T tmp = y0;
    y0 = x1;
    x1 = tmp;
  }

  bool getInverse(M2<T> *inv)
  {
    bool result = false;

    BREAKABLE_START
    {
      T det = (x0 * y1) - (x1 * y0);

      if (det == 0) break;

      T tmp;

      tmp = x0;
      inv->x0 = inv_det * y1;
      inv->y1 = inv_det * tmp;

      tmp = x1;
      inv->x1 = - inv_det * y0;
      inv->y0 = - inv_det * tmp;

      result = true;
    }
    BREAKABLE_END

    return result;
  }

  bool invert()
  {
    return getInverse(this);
  }
};

template <typename T>
M2<T> m2(T x0, T x1,
         T y0, T y1)
{
  M2<T> result;
  result.x0 = x0;
  result.y0 = y0;
  result.x1 = x1;
  result.y1 = y1;
  return result;
}

template <typename T, typename U>
M2<T> m2(M2<U> m)
{
  M2<T> result;
  result.x0 = m.x0;
  result.y0 = m.y0;
  result.x1 = m.x1;
  result.y1 = m.y1;
  return result;
}

template <typename T>
bool operator ==(const M2<T> &a, const M2<T> &b)
{
  return (a.x0 == b.x0) &&
         (a.y0 == b.y0) &&
         (a.x1 == b.x1) &&
         (a.y1 == b.y1);
}

template <typename T>
bool operator !=(const M2<T> &a, const M2<T> &b)
{
  return !(a == b);
}



///////////
// 3 x 3 //
///////////

template <typename T>
using M3 = M<3,T>;

template<typename T>
struct Mx<3,3,T>
{
  union
  {
    struct
    {
      T x0, y0, z0,
        x1, y1, z1,
        x3, y3, z3;
    };
    T     v[9];
    V3<T> cols[3];
  };

  static constexpr M3<T> identity()
  {
    return m3<T>(1.0f, 0.0f, 0.0f,
                 0.0f, 1.0f, 0.0f,
                 0.0f, 0.0f, 1.0f);
  }

  V3<T> row(u32 i) const
  {
    return v3(cols[0].v[i], cols[1].v[i], cols[2].v[i]);
  }

  M3<T> transposed()
  {
    M3<T> result;

    result.x0 = x0;
    result.y0 = x1;
    result.z0 = x2;
    result.x1 = y0;
    result.y1 = y1;
    result.z1 = y2;
    result.x2 = z0;
    result.y2 = z1;
    result.z2 = z2;

    return result;
  }

  void transpose()
  {
    T tmp;

    tmp = y0;
    y0 = x1;
    x1 = tmp;

    tmp = z0;
    z0 = x2;
    x2 = tmp;

    tmp = z1;
    z1 = y2;
    y2 = tmp;
  }

  // TODO:
  //
  // bool getInverse(M3<T> *inv)
  // {
  // }
  //
  // bool invert()
  // {
  //   return getInverse(this);
  // }
};

template <typename T>
M3<T> m3(T x0, T x1, T x2,
         T y0, T y1, T y2,
         T z0, T z1, T z2)
{
  M3<T> result;
  result.x0 = x0;
  result.y0 = y0;
  result.z0 = z0;
  result.x1 = x1;
  result.y1 = y1;
  result.z1 = z1;
  result.x2 = x2;
  result.y2 = y2;
  result.z2 = z2;
  return result;
}

template <typename T, typename U>
M3<T> m3(M3<U> m)
{
  M3<T> result;
  result.x0 = m.x0;
  result.y0 = m.y0;
  result.z0 = m.z0;
  result.x1 = m.x1;
  result.y1 = m.y1;
  result.z1 = m.z1;
  result.x2 = m.x2;
  result.y2 = m.y2;
  result.z2 = m.z2;
  return result;
}

template <typename T>
bool operator ==(const M3<T> &a, const M3<T> &b)
{
  return (a.x0 == b.x0) &&
         (a.y0 == b.y0) &&
         (a.z0 == b.z0) &&
         (a.x1 == b.x1) &&
         (a.y1 == b.y1) &&
         (a.z1 == b.z1) &&
         (a.x2 == b.x2) &&
         (a.y2 == b.y2) &&
         (a.z2 == b.z2);
}

template <typename T>
bool operator !=(const M3<T> &a, const M3<T> &b)
{
  return !(a == b);
}



///////////
// 4 x 4 //
///////////

template <typename T>
using M4 = M<4,T>;

template<typename T>
struct Mx<4,4,T>
{
  union
  {
    struct
    {
      T x0, y0, z0, w0,
        x1, y1, z1, w1,
        x2, y2, z2, w2,
        x3, y3, z3, w3;
    };
    T     v[16];
    V4<T> cols[4];
  };

  static constexpr M4<T> identity()
  {
    return m4<T>(1.0f, 0.0f, 0.0f, 0.0f,
                 0.0f, 1.0f, 0.0f, 0.0f,
                 0.0f, 0.0f, 1.0f, 0.0f,
                 0.0f, 0.0f, 0.0f, 1.0f);
  }

  V4<T> row(u32 i) const
  {
    return v4(cols[0].v[i], cols[1].v[i], cols[2].v[i], cols[3].v[i]);
  }

  M4<T> transposed()
  {
    M4<T> result;

    result.x0 = x0;
    result.y0 = x1;
    result.z0 = x2;
    result.w0 = x3;
    result.x1 = y0;
    result.y1 = y1;
    result.z1 = y2;
    result.w1 = y3;
    result.x2 = z0;
    result.y2 = z1;
    result.z2 = z2;
    result.w2 = z3;
    result.x3 = w0;
    result.y3 = w1;
    result.z3 = w2;
    result.w3 = w3;

    return result;
  }

  void transpose()
  {
    T tmp;

    tmp = y0;
    y0 = x1;
    x1 = tmp;

    tmp = z0;
    z0 = x2;
    x2 = tmp;

    tmp = w0;
    w0 = x3;
    x3 = tmp;

    tmp = z1;
    z1 = y2;
    y2 = tmp;

    tmp = w1;
    w1 = y3;
    y3 = tmp;

    tmp = w2;
    w2 = z3;
    z3 = tmp;
  }

  template <typename T>
  bool getInverse(M4<T> *inv)
  {
    bool result = false;

    BREAKABLE_START
    {
      T det, tmp[16];

      tmp[0] = v[5]  * v[10] * v[15] -
               v[5]  * v[11] * v[14] -
               v[9]  * v[6]  * v[15] +
               v[9]  * v[7]  * v[14] +
               v[13] * v[6]  * v[11] -
               v[13] * v[7]  * v[10];

      tmp[4] = -v[4]  * v[10] * v[15] +
                v[4]  * v[11] * v[14] +
                v[8]  * v[6]  * v[15] -
                v[8]  * v[7]  * v[14] -
                v[12] * v[6]  * v[11] +
                v[12] * v[7]  * v[10];

      tmp[8] = v[4]  * v[9]  * v[15] -
               v[4]  * v[11] * v[13] -
               v[8]  * v[5]  * v[15] +
               v[8]  * v[7]  * v[13] +
               v[12] * v[5]  * v[11] -
               v[12] * v[7]  * v[9];

      tmp[12] = -v[4]  * v[9]  * v[14] +
                 v[4]  * v[10] * v[13] +
                 v[8]  * v[5]  * v[14] -
                 v[8]  * v[6]  * v[13] -
                 v[12] * v[5]  * v[10] +
                 v[12] * v[6]  * v[9];

      det = v[0] * tmp[0] + v[1] * tmp[4] + v[2] * tmp[8] + v[3] * tmp[12];

      if (det == 0) break;

      tmp[1] = -v[1]  * v[10] * v[15] +
                v[1]  * v[11] * v[14] +
                v[9]  * v[2]  * v[15] -
                v[9]  * v[3]  * v[14] -
                v[13] * v[2]  * v[11] +
                v[13] * v[3]  * v[10];

      tmp[5] = v[0]  * v[10] * v[15] -
               v[0]  * v[11] * v[14] -
               v[8]  * v[2]  * v[15] +
               v[8]  * v[3]  * v[14] +
               v[12] * v[2]  * v[11] -
               v[12] * v[3]  * v[10];

      tmp[9] = -v[0]  * v[9]  * v[15] +
                v[0]  * v[11] * v[13] +
                v[8]  * v[1]  * v[15] -
                v[8]  * v[3]  * v[13] -
                v[12] * v[1]  * v[11] +
                v[12] * v[3]  * v[9];

      tmp[13] = v[0]  * v[9]  * v[14] -
                v[0]  * v[10] * v[13] -
                v[8]  * v[1]  * v[14] +
                v[8]  * v[2]  * v[13] +
                v[12] * v[1]  * v[10] -
                v[12] * v[2]  * v[9];

      tmp[2] = v[1]  * v[6] * v[15] -
               v[1]  * v[7] * v[14] -
               v[5]  * v[2] * v[15] +
               v[5]  * v[3] * v[14] +
               v[13] * v[2] * v[7] -
               v[13] * v[3] * v[6];

      tmp[6] = -v[0]  * v[6] * v[15] +
                v[0]  * v[7] * v[14] +
                v[4]  * v[2] * v[15] -
                v[4]  * v[3] * v[14] -
                v[12] * v[2] * v[7] +
                v[12] * v[3] * v[6];

      tmp[10] = v[0]  * v[5] * v[15] -
                v[0]  * v[7] * v[13] -
                v[4]  * v[1] * v[15] +
                v[4]  * v[3] * v[13] +
                v[12] * v[1] * v[7] -
                v[12] * v[3] * v[5];

      tmp[14] = -v[0]  * v[5] * v[14] +
                 v[0]  * v[6] * v[13] +
                 v[4]  * v[1] * v[14] -
                 v[4]  * v[2] * v[13] -
                 v[12] * v[1] * v[6] +
                 v[12] * v[2] * v[5];

      tmp[3] = -v[1] * v[6] * v[11] +
                v[1] * v[7] * v[10] +
                v[5] * v[2] * v[11] -
                v[5] * v[3] * v[10] -
                v[9] * v[2] * v[7] +
                v[9] * v[3] * v[6];

      tmp[7] = v[0] * v[6] * v[11] -
               v[0] * v[7] * v[10] -
               v[4] * v[2] * v[11] +
               v[4] * v[3] * v[10] +
               v[8] * v[2] * v[7] -
               v[8] * v[3] * v[6];

      tmp[11] = -v[0] * v[5] * v[11] +
                 v[0] * v[7] * v[9] +
                 v[4] * v[1] * v[11] -
                 v[4] * v[3] * v[9] -
                 v[8] * v[1] * v[7] +
                 v[8] * v[3] * v[5];

      tmp[15] = v[0] * v[5] * v[10] -
                v[0] * v[6] * v[9] -
                v[4] * v[1] * v[10] +
                v[4] * v[2] * v[9] +
                v[8] * v[1] * v[6] -
                v[8] * v[2] * v[5];

      T inv_det = 1.0 / det;

      for (u32 i = 0; i < 16; ++i)
      {
        inv->v[i] = inv_det * tmp[i];
      }

      result = true;
    }
    BREAKABLE_END

    return result;
  }

  bool invert()
  {
    return getInverse(this);
  }
};

template <typename T>
M4<T> m4(T x0, T x1, T x2, T x3,
         T y0, T y1, T y2, T y3,
         T z0, T z1, T z2, T z3,
         T w0, T w1, T w2, T w3)
{
  M4<T> result;
  result.x0 = x0;
  result.y0 = y0;
  result.z0 = z0;
  result.w0 = w0;
  result.x1 = x1;
  result.y1 = y1;
  result.z1 = z1;
  result.w1 = w1;
  result.x2 = x2;
  result.y2 = y2;
  result.z2 = z2;
  result.w2 = w2;
  result.x3 = x3;
  result.y3 = y3;
  result.z3 = z3;
  result.w3 = w3;
  return result;
}

template <typename T, typename U>
M4<T> m4(M4<U> m)
{
  M4<T> result;
  result.x0 = m.x0;
  result.y0 = m.y0;
  result.z0 = m.z0;
  result.w0 = m.w0;
  result.x1 = m.x1;
  result.y1 = m.y1;
  result.z1 = m.z1;
  result.w1 = m.w1;
  result.x2 = m.x2;
  result.y2 = m.y2;
  result.z2 = m.z2;
  result.w2 = m.w2;
  result.x3 = m.x3;
  result.y3 = m.y3;
  result.z3 = m.z3;
  result.w3 = m.w3;
  return result;
}

template <typename T>
bool operator ==(const M4<T> &a, const M4<T> &b)
{
  return (a.x0 == b.x0) &&
         (a.y0 == b.y0) &&
         (a.z0 == b.z0) &&
         (a.w0 == b.w0) &&
         (a.x1 == b.x1) &&
         (a.y1 == b.y1) &&
         (a.z1 == b.z1) &&
         (a.w1 == b.w1) &&
         (a.x2 == b.x2) &&
         (a.y2 == b.y2) &&
         (a.z2 == b.z2) &&
         (a.w2 == b.w2) &&
         (a.x3 == b.x3) &&
         (a.y3 == b.y3) &&
         (a.z3 == b.z3) &&
         (a.w3 == b.w3);
}

template <typename T>
bool operator !=(const M4<T> &a, const M4<T> &b)
{
  return !(a == b);
}



////////////////////
// Multiplication //
////////////////////


//////////////////
// M2 * V2 = V2 //
//////////////////

template <typename T>
V2<T> operator *(const M2<T> &m, const V2<T> &v)
{
  return (m.cols[0] * v.x) + (m.cols[1] * v.y);
}

template <typename T>
V2<T> &operator *=(V2<T> &b, const M2<T> &a)
{
  b = a * b;
  return b;
}


//////////////////
// M2 * M2 = M2 //
//////////////////

template <typename T>
M2<T> operator *(const M2<T> &a, const M2<T> &b)
{
  // FIXME: I assume this transposed copy optimises out?
  V2<T> a_rows[2] = { row(a, 0), row(a, 1) };
  return m2(dot(&a_rows[0], &b.cols[0]), dot(&a_rows[0], &b.cols[1]),
            dot(&a_rows[1], &b.cols[0]), dot(&a_rows[1], &b.cols[1]));
}

template <typename T>
M2<T> &operator *=(M2<T> &b, const M2<T> &a)
{
  b = a * b;
  return b;
}


//////////////////
// M3 * V3 = V3 //
//////////////////

template <typename T>
V3<T> operator *(const M3<T> &m, const V3<T> &v)
{
  return (m.cols[0] * v.x) + (m.cols[1] * v.y) + (m.cols[2] * v.z);
}

template <typename T>
V3<T> &operator *=(V3<T> &a, const M3<T> &b)
{
  a = b * a;
  return a;
}


//////////////////
// M3 * M3 = M3 //
//////////////////

template <typename T>
M3<T> operator *(const M3<T> &a, const M3<T> &b)
{
  // FIXME: I assume this transposed copy optimises out?
  V3<T> a_rows[3] = { a.row(0), a.row(1), a.row(2) };
  return m3(dot(a_rows[0], b.cols[0]), dot(a_rows[0], b.cols[1]), dot(a_rows[0], b.cols[2]),
            dot(a_rows[1], b.cols[0]), dot(a_rows[1], b.cols[1]), dot(a_rows[1], b.cols[2]),
            dot(a_rows[2], b.cols[0]), dot(a_rows[2], b.cols[1]), dot(a_rows[2], b.cols[2]));
}

template <typename T>
M3<T> &operator *=(M3<T> &a, const M3<T> &b)
{
  a = b * a;
  return a;
}


//////////////////
// M4 * V4 = V4 //
//////////////////

template <typename T>
V4<T> operator *(const M4<T> &m, const V4<T> &v)
{
  return (m.cols[0] * v.x) + (m.cols[1] * v.y) + (m.cols[2] * v.z) + (m.cols[3] * v.w);
}

template <typename T>
V4<T> &operator *=(V4<T> &a, const M4<T> &b)
{
  a = b * a;
  return a;
}


//////////////////
// M4 * M4 = M4 //
//////////////////

template <typename T>
M4<T> operator *(const M4<T> &a, const M4<T> &b)
{
  // FIXME: I assume this transposed copy optimises out?
  V4<T> a_rows[4] = { a.row(0), a.row(1), a.row(2), a.row(3) };
  return m4(dot(a_rows[0], b.cols[0]), dot(a_rows[0], b.cols[1]), dot(a_rows[0], b.cols[2]), dot(a_rows[0], b.cols[3]),
            dot(a_rows[1], b.cols[0]), dot(a_rows[1], b.cols[1]), dot(a_rows[1], b.cols[2]), dot(a_rows[1], b.cols[3]),
            dot(a_rows[2], b.cols[0]), dot(a_rows[2], b.cols[1]), dot(a_rows[2], b.cols[2]), dot(a_rows[2], b.cols[3]),
            dot(a_rows[3], b.cols[0]), dot(a_rows[3], b.cols[1]), dot(a_rows[3], b.cols[2]), dot(a_rows[3], b.cols[3]));
}

template <typename T>
M4<T> &operator *=(M4<T> &a, const M4<T> &b)
{
  a = b * a;
  return a;
}
