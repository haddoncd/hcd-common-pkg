#ifdef common_checks_hpp
#error Multiple inclusion
#endif
#define common_checks_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#include <type_traits>

bool checks_passed = true;

#define STRINGIFY_LINE_NUM(line) #line
#define STRINGIFY_LINE_NUM_INDIRECT(line) STRINGIFY_LINE_NUM(line)
#define LINE_NUM_STRING STRINGIFY_LINE_NUM_INDIRECT(__LINE__)


#define CHECK_TRUE(expr)                       \
  do                                           \
  {                                            \
    bool actual = expr;                        \
    if (!actual)                               \
    {                                          \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n" \
             "      " #expr " -> false\n",     \
             __FUNCTION__);                    \
      checks_passed = false;                   \
    }                                          \
  }                                            \
  while (false)

#define CHECK_FALSE(expr)                      \
  do                                           \
  {                                            \
    bool actual = expr;                        \
    if (actual)                                \
    {                                          \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n" \
             "      " #expr " -> true\n",      \
             __FUNCTION__);                    \
      checks_passed = false;                   \
    }                                          \
  }                                            \
  while (false)

#define CHECK_BOOL(expr, expected)                \
  do                                              \
  {                                               \
    bool actual = expr;                           \
    bool actual_expected = expected;              \
    if (actual != actual_expected)                \
    {                                             \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"    \
             "      " #expr " -> %s != %s <- "    \
             #expected "\n",                      \
             __FUNCTION__,                        \
             actual ? "true" : "false",           \
             actual_expected ? "true" : "false"); \
      checks_passed = false;                      \
    }                                             \
  }                                               \
  while (false)


#define CHECK_GENERIC(expr, expected, type, format)           \
  do                                                          \
  {                                                           \
    auto actual = expr;                                       \
    static_assert(std::is_same<decltype(actual),type>::value, \
                  "Type error in CHECK macro");               \
    type actual_expected = (type) (expected);                 \
    if (actual != actual_expected)                            \
    {                                                         \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"                \
             "      " #expr " -> " format                     \
             " != " format " (<- " #expected ")\n",           \
             __FUNCTION__,                                    \
             actual,                                          \
             actual_expected);                                \
      checks_passed = false;                                  \
    }                                                         \
  }                                                           \
  while (false)

#define CHECK_GENERIC_NOT(expr, expected, type, format)       \
  do                                                          \
  {                                                           \
    auto actual = expr;                                       \
    static_assert(std::is_same<decltype(actual),type>::value, \
                  "Type error in CHECK macro");               \
    type actual_expected = (type) (expected);                 \
    if (actual == actual_expected)                            \
    {                                                         \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"                \
             "      " #expr " -> " format                     \
             " == " format " (<- " #expected ")\n",           \
             __FUNCTION__,                                    \
             actual,                                          \
             actual_expected);                                \
      checks_passed = false;                                  \
    }                                                         \
  }                                                           \
  while (false)

#define CHECK_U8(      expr, expected) CHECK_GENERIC(    expr, expected,  u8,  "%u")
#define CHECK_U8_NOT(  expr, expected) CHECK_GENERIC_NOT(expr, expected,  u8,  "%u")
#define CHECK_I8(      expr, expected) CHECK_GENERIC(    expr, expected,  i8,  "%d")
#define CHECK_I8_NOT(  expr, expected) CHECK_GENERIC_NOT(expr, expected,  i8,  "%d")
#define CHECK_U32(     expr, expected) CHECK_GENERIC(    expr, expected, u32,  "%u")
#define CHECK_U32_NOT( expr, expected) CHECK_GENERIC_NOT(expr, expected, u32,  "%u")
#define CHECK_I32(     expr, expected) CHECK_GENERIC(    expr, expected, i32,  "%d")
#define CHECK_I32_NOT( expr, expected) CHECK_GENERIC_NOT(expr, expected, i32,  "%d")
#define CHECK_U64(     expr, expected) CHECK_GENERIC(    expr, expected, u64,  "%I64u")
#define CHECK_U64_NOT( expr, expected) CHECK_GENERIC_NOT(expr, expected, u64,  "%I64u")
#define CHECK_I64(     expr, expected) CHECK_GENERIC(    expr, expected, i64,  "%I64d")
#define CHECK_I64_NOT( expr, expected) CHECK_GENERIC_NOT(expr, expected, i64,  "%I64d")
#define CHECK_CHAR(    expr, expected) CHECK_GENERIC(    expr, expected, char, "%c")
#define CHECK_CHAR_NOT(expr, expected) CHECK_GENERIC_NOT(expr, expected, char, "%c")

#define CHECK_F32_EXACT(    expr, expected) CHECK_GENERIC(    expr, expected, f32, "%g")
#define CHECK_F32_EXACT_NOT(expr, expected) CHECK_GENERIC_NOT(expr, expected, f32, "%g")
#define CHECK_F64_EXACT(    expr, expected) CHECK_GENERIC(    expr, expected, f64, "%g")
#define CHECK_F64_EXACT_NOT(expr, expected) CHECK_GENERIC_NOT(expr, expected, f64, "%g")


#define CHECK_GENERIC_DELTA(expr, expected, delta, type, format)  \
  do                                                              \
  {                                                               \
    auto actual = expr;                                           \
    static_assert(std::is_same<decltype(actual),type>::value,     \
                  "Type error in CHECK macro");                   \
    type actual_expected = (type) (expected);                     \
    type actual_delta    = abs((type) (delta));                   \
    if (actual_delta < absDiff(actual, actual_expected))          \
    {                                                             \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"                    \
             "      " #expr " -> " format                         \
             " != " format " (<- " #expected ")"                  \
             " (+- " format " <- " #delta ")\n",                  \
             __FUNCTION__,                                        \
             actual,                                              \
             actual_expected),                                    \
             actual_delta);                                       \
      checks_passed = false;                                      \
    }                                                             \
  }                                                               \
  while (false)

#define CHECK_GENERIC_DELTA_NOT(expr, expected, delta, type, format) \
  do                                                                 \
  {                                                                  \
    auto actual = expr;                                              \
    static_assert(std::is_same<decltype(actual),type>::value,        \
                  "Type error in CHECK macro");                      \
    type actual_expected = (type) (expected);                        \
    type actual_delta    = abs((type) (delta));                      \
    if (actual_delta >= absDiff(actual, actual_expected))            \
    {                                                                \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"                       \
             "      " #expr " -> " format                            \
             " == " format " (<- " #expected ")"                     \
             " (+- " format " <- " #delta ")\n",                     \
             __FUNCTION__,                                           \
             actual,                                                 \
             actual_expected),                                       \
             actual_delta);                                          \
      checks_passed = false;                                         \
    }                                                                \
  }                                                                  \
  while (false)

#define CHECK_F32(    expr, expected, delta) CHECK_GENERIC_DELTA(    expr, expected, delta, f32, "%g")
#define CHECK_F32_NOT(expr, expected, delta) CHECK_GENERIC_DELTA_NOT(expr, expected, delta, f32, "%g")
#define CHECK_F64(    expr, expected, delta) CHECK_GENERIC_DELTA(    expr, expected, delta, f64, "%g")
#define CHECK_F64_NOT(expr, expected, delta) CHECK_GENERIC_DELTA_NOT(expr, expected, delta, f64, "%g")



#define CHECK_POINTER(expr, expected)                        \
  do                                                         \
  {                                                          \
    void *actual = (void *) (expr);                          \
    if (actual != (void *) (expected))                       \
    {                                                        \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"               \
             "      " #expr " -> %#I64x != " #expected "\n", \
             __FUNCTION__,                                   \
             (uintptr_t) actual);                            \
      checks_passed = false;                                 \
    }                                                        \
  }                                                          \
  while (false)

#define CHECK_POINTER_NOT(expr, expected)                    \
  do                                                         \
  {                                                          \
    void *actual = (void *) (expr);                          \
    if (actual == (void *) (expected))                       \
    {                                                        \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"               \
             "      " #expr " -> %#I64x == " #expected "\n", \
             __FUNCTION__,                                   \
             (uintptr_t) actual);                            \
      checks_passed = false;                                 \
    }                                                        \
  }                                                          \
  while (false)

#define CHECK_NULL(expr)     CHECK_POINTER(expr, NULL)
#define CHECK_NOT_NULL(expr) CHECK_POINTER_NOT(expr, NULL)


#define CHECK_ENUM_GENERIC(expr, expected, type, format) \
  do                                                     \
  {                                                      \
    type::Enum actual = expr;                            \
    if (actual != type::expected)                        \
    {                                                    \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"           \
             "      " #expr " -> " format                \
             " != " format " <- " #expected "\n",        \
             __FUNCTION__,                               \
             type::expected,                             \
             actual);                                    \
      checks_passed = false;                             \
    }                                                    \
  }                                                      \
  while (false)

#define CHECK_ENUM_GENERIC_NOT(expr, expected, type, format)       \
  do                                                               \
  {                                                                \
    type::Enum actual = expr;                                      \
    if (actual == type::expected)                                  \
    {                                                              \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"                     \
             "      " #expr " == " format " (== " #expected ")\n", \
             __FUNCTION__,                                         \
             actual);                                              \
      checks_passed = false;                                       \
    }                                                              \
  }                                                                \
  while (false)

#define CHECK_ENUM_I32(    expr, expected, type) CHECK_ENUM_GENERIC(    expr, expected, type, "%d")
#define CHECK_ENUM_I32_NOT(expr, expected, type) CHECK_ENUM_GENERIC_NOT(expr, expected, type, "%d")

#define CHECK_ENUM     CHECK_ENUM_I32
#define CHECK_ENUM_NOT CHECK_ENUM_I32_NOT
