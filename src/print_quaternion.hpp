#ifdef print_quaternion_hpp
#error Multiple inclusion
#endif
#define print_quaternion_hpp

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

#ifndef print_vector_hpp
#error "Please include print_vector.hpp before this file"
#endif

#ifndef quaternion_hpp
#error "Please include quaternion.hpp before this file"
#endif



template <typename S, typename T>
bool print(S out, Q<T> q)
{
  return print(out, "("_s, q.v, ", "_s, fmt(q.s), ")"_s);
}
