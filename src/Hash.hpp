#ifdef Hash_hpp
#error Multiple inclusion
#endif
#define Hash_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif


struct Hash32
{
  static constexpr u32 PRIME = 16777619;
  static constexpr Hash32 SEED() { return Hash32 { 0x6D846187 }; }

  u32 value;

  Hash32 hash(u8 data) const;

  Hash32 hash(u8 * data, u32 size) const;

  template <typename T>
  Hash32 hash(T data) const
  {
    static_assert(std::is_fundamental<T>::value ||
                  std::is_enum<T>::value,
                  "No hash function defined for non-trivial type");
    return hash((u8 *) &data, sizeof(data));
  }

  u32 fold(u8 hash_size);

  template <u32 N>
  typename std::enable_if<N < 32, u32>::type
  fold()
  {
    u32 mask = (1u << N) - 1;
    u32 upper = value >> N;
    u32 result = mask & (value ^ upper);
    return result;
  }

  template <u32 N>
  typename std::enable_if<N == 32, u32>::type
  fold()
  {
    return value;
  }
};


struct Hash64
{
  static constexpr u64 PRIME = 1099511628211ull;
  static constexpr Hash64 SEED() { return Hash64 { 0x23CE3DF6F484B1ECull }; }

  u64 value;

  Hash64 hash(u8 data) const;

  Hash64 hash(u8 * data, u32 size) const;

  template <typename T>
  Hash64 hash(T data) const
  {
    return hash((u8 *) &data, sizeof(data));
  }

  u32 fold32(u8 hash_size);
  u64 fold64(u8 hash_size);

  template <u32 N>
  typename std::enable_if<N <= 32, u32>::type
  fold()
  {
    u32 mask = (1u << N) - 1;
    u32 upper = value >> N;
    u32 result = mask & (value ^ upper);
    return result;
  }

  template <u32 N>
  typename std::enable_if<32 < N && N < 64, u64>::type
  fold()
  {
    u64 mask = (1ull << N) - 1;
    u64 upper = value >> N;
    u64 result = mask & (value ^ upper);
    return result;
  }

  template <u32 N>
  typename std::enable_if<N == 64, u64>::type
  fold()
  {
    return value;
  }
};
