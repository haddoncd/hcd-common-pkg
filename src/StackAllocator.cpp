#ifdef StackAllocator_cpp
#error Multiple inclusion
#endif
#define StackAllocator_cpp

#ifndef StackAllocator_hpp
#include "StackAllocator.hpp"
#endif


void StackAllocator::init(u8 *memory, size_t size)
{
  base = memory;
  firstByteBeyond = memory + size;
  firstFree = base;
}

void StackAllocator::clear()
{
  firstFree = base;
}

u8 *allocUninitRaw(StackAllocator *stack, size_t size, size_t alignment)
{
  u8 *result = align(stack->firstFree, alignment);
  stack->firstFree = result + size;
  assert(stack->firstFree <= stack->firstByteBeyond);
  return result;
}

u8 *allocRaw(StackAllocator *stack, size_t size, size_t alignment)
{
  u8 *result = allocUninitRaw(stack, size, alignment);
  zeroArray(result, size);
  return result;
}
