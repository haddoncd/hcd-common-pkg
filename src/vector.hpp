#ifdef vector_hpp
#error Multiple inclusion
#endif
#define vector_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif



template <typename V>
void normalize(V *v)
{
  auto length_squared = dot(*v, *v);
  if (length_squared == 0)
  {
    *v = {};
  }
  else
  {
    *v *= 1.0f / sqrt(length_squared);
  }
}


template <typename T>
struct V2
{
  union
  {
    struct
    {
      T x;
      T y;
    };
    T v[2];
  };
};

template <typename T>
V2<T> v2(T x, T y)
{
  V2<T> result;
  result.x = x;
  result.y = y;
  return result;
}

template <typename T, typename U, typename V>
V2<T> v2(U x, V y)
{
  V2<T> result;
  result.x = x;
  result.y = y;
  return result;
}

template <typename T>
V2<T> v2(T xy)
{
  V2<T> result;
  result.x = xy;
  result.y = xy;
  return result;
}

template <typename T, typename U>
V2<T> v2(U xy)
{
  V2<T> result;
  result.x = xy;
  result.y = xy;
  return result;
}

template <typename T, typename U>
V2<T> v2(V2<U> v)
{
  V2<T> result;
  result.x = v.x;
  result.y = v.y;
  return result;
}

template <typename T>
bool operator ==(const V2<T> &a, const V2<T> &b)
{
  return (a.x == b.x) &&
         (a.y == b.y);
}

template <typename T>
bool operator !=(const V2<T> &a, const V2<T> &b)
{
  return !(a == b);
}

template <typename T>
V2<T> operator +(const V2<T> &a, const V2<T> &b)
{
  V2<T> result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  return result;
}

template <typename T>
V2<T> &operator +=(V2<T> &a, const V2<T> &b)
{
  a.x += b.x;
  a.y += b.y;
  return a;
}

template <typename T>
V2<T> operator -(const V2<T> &a, const V2<T> &b)
{
  V2<T> result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  return result;
}

template <typename T>
V2<T> operator -=(V2<T> &a, const V2<T> &b)
{
  a.x -= b.x;
  a.y -= b.y;
  return a;
}

template <typename T>
V2<T> operator -(const V2<T> &v)
{
  V2<T> result;
  result.x = -v.x;
  result.y = -v.y;
  return result;
}

template <typename T>
V2<T> operator *(V2<T> v, T s)
{
  V2<T> result;
  result.x = v.x * s;
  result.y = v.y * s;
  return result;
}

template <typename T>
V2<T> operator *(T s, V2<T> v)
{
  return v * s;
}

template <typename T>
V2<T> &operator *=(V2<T> &v, const T &s)
{
  v.x *= s;
  v.y *= s;
  return v;
}

template <typename T>
V2<T> operator /(V2<T> v, T s)
{
  V2<T> result;
  result.x = v.x / s;
  result.y = v.y / s;
  return result;
}

template <typename T>
V2<T> &operator /=(V2<T> &v, const T &s)
{
  v.x /= s;
  v.y /= s;
  return v;
}

template <typename T>
T dot(const V2<T> a, const V2<T> b)
{
  return (a.x * b.x) +
         (a.y * b.y);
}

template <typename T>
V2<T> hadamard(const V2<T> a, const V2<T> b)
{
  return v2(a.x * b.x,
            a.y * b.y);
}



template <typename T>
struct V3
{
  union
  {
    struct
    {
      T x;
      T y;
      T z;
    };
    struct
    {
      T r;
      T g;
      T b;
    };
    T v[3];
    V2<T> xy;
  };
};

template <typename T>
V3<T> v3(T x, T y, T z)
{
  V3<T> result;
  result.x = x;
  result.y = y;
  result.z = z;
  return result;
}

template <typename T, typename U, typename V, typename W>
V3<T> v3(U x, V y, W z)
{
  V3<T> result;
  result.x = x;
  result.y = y;
  result.z = z;
  return result;
}

template <typename T>
V3<T> v3(T xyz)
{
  V3<T> result;
  result.x = xyz;
  result.y = xyz;
  result.z = xyz;
  return result;
}

template <typename T, typename U>
V3<T> v3(U xyz)
{
  V3<T> result;
  result.x = xyz;
  result.y = xyz;
  result.z = xyz;
  return result;
}

template <typename T>
V3<T> v3(V2<T> xy, T z)
{
  V3<T> result;
  result.x = xy.v[0];
  result.y = xy.v[1];
  result.z = z;
  return result;
}

template <typename T, typename U, typename V>
V3<T> v3(V2<U> xy, V z)
{
  V3<T> result;
  result.x = xy.v[0];
  result.y = xy.v[1];
  result.z = z;
  return result;
}

template <typename T>
V3<T> v3(T x, V2<T> yz)
{
  V3<T> result;
  result.x = x;
  result.y = yz.v[0];
  result.z = yz.v[1];
  return result;
}

template <typename T, typename U, typename V>
V3<T> v3(U x, V2<V> yz)
{
  V3<T> result;
  result.x = x;
  result.y = yz.v[0];
  result.z = yz.v[1];
  return result;
}

template <typename T, typename U>
V3<T> v3(V3<U> v)
{
  V3<T> result;
  result.x = v.x;
  result.y = v.y;
  result.z = v.z;
  return result;
}

template <typename T>
bool operator ==(const V3<T> &a, const V3<T> &b)
{
  return (a.x == b.x) &&
         (a.y == b.y) &&
         (a.z == b.z);
}

template <typename T>
bool operator !=(const V3<T> &a, const V3<T> &b)
{
  return !(a == b);
}

template <typename T>
V3<T> operator +(const V3<T> &a, const V3<T> &b)
{
  V3<T> result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  result.z = a.z + b.z;
  return result;
}

template <typename T>
V3<T> &operator +=(V3<T> &a, const V3<T> &b)
{
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  return a;
}

template <typename T>
V3<T> operator -(const V3<T> &a, const V3<T> &b)
{
  V3<T> result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  result.z = a.z - b.z;
  return result;
}

template <typename T>
V3<T> operator -=(V3<T> &a, const V3<T> &b)
{
  a.x -= b.x;
  a.y -= b.y;
  a.z -= b.z;
  return a;
}

template <typename T>
V3<T> operator -(const V3<T> &v)
{
  V3<T> result;
  result.x = -v.x;
  result.y = -v.y;
  result.z = -v.z;
  return result;
}

template <typename T>
V3<T> operator *(V3<T> v, T s)
{
  V3<T> result;
  result.x = v.x * s;
  result.y = v.y * s;
  result.z = v.z * s;
  return result;
}

template <typename T>
V3<T> operator *(T s, V3<T> v)
{
  return v * s;
}

template <typename T>
V3<T> &operator *=(V3<T> &v, const T &s)
{
  v.x *= s;
  v.y *= s;
  v.z *= s;
  return v;
}

template <typename T>
V3<T> operator /(V3<T> v, T s)
{
  V3<T> result;
  result.x = v.x / s;
  result.y = v.y / s;
  result.z = v.z / s;
  return result;
}

template <typename T>
V3<T> &operator /=(V3<T> &v, const T &s)
{
  v.x /= s;
  v.y /= s;
  v.z /= s;
  return v;
}

template <typename T>
T dot(const V3<T> a, const V3<T> b)
{
  return (a.x * b.x) +
         (a.y * b.y) +
         (a.z * b.z);
}

template <typename T>
V3<T> cross(const V3<T> a, const V3<T> b)
{
  return v3((a.y * b.z) - (a.z * b.y),
            (a.z * b.x) - (a.x * b.z),
            (a.x * b.y) - (a.y * b.x));
}

template <typename T>
V3<T> hadamard(const V3<T> a, const V3<T> b)
{
  return v3(a.x * b.x,
            a.y * b.y,
            a.z * b.z);
}


template <typename T>
struct V4
{
  union
  {
    struct
    {
      T x;
      T y;
      T z;
      T w;
    };
    struct
    {
      T r;
      T g;
      T b;
      T a;
    };
    T v[4];
    V2<T> xy;
    V3<T> xyz;
    V3<T> rgb;
  };
};

template <typename T>
V4<T> v4(T x, T y, T z, T w)
{
  V4<T> result;
  result.x = x;
  result.y = y;
  result.z = z;
  result.w = w;
  return result;
}

template <typename T, typename U, typename V, typename W, typename X>
V4<T> v4(U x, V y, W z, X w)
{
  V4<T> result;
  result.x = x;
  result.y = y;
  result.z = z;
  result.w = w;
  return result;
}

template <typename T>
V4<T> v4(V2<T> xy, T z, T w)
{
  V4<T> result;
  result.x = xy.v[0];
  result.y = xy.v[1];
  result.z = z;
  result.w = w;
  return result;
}

template <typename T, typename U, typename V, typename W>
V4<T> v4(V2<U> xy, V z, W w)
{
  V4<T> result;
  result.x = xy.v[0];
  result.y = xy.v[1];
  result.z = z;
  result.w = w;
  return result;
}

template <typename T>
V4<T> v4(V2<T> xy, V2<T> zw)
{
  V4<T> result;
  result.x = xy.v[0];
  result.y = xy.v[1];
  result.z = zw.v[0];
  result.w = zw.v[1];
  return result;
}

template <typename T, typename U, typename V>
V4<T> v4(V2<U> xy, V2<V> zw)
{
  V4<T> result;
  result.x = xy.v[0];
  result.y = xy.v[1];
  result.z = zw.v[0];
  result.w = zw.v[1];
  return result;
}

template <typename T>
V4<T> v4(T x, T y, V2<T> zw)
{
  V4<T> result;
  result.x = x;
  result.y = y;
  result.z = zw.v[0];
  result.w = zw.v[1];
  return result;
}

template <typename T, typename U, typename V, typename W>
V4<T> v4(U x, V y, V2<W> zw)
{
  V4<T> result;
  result.x = x;
  result.y = y;
  result.z = zw.v[0];
  result.w = zw.v[1];
  return result;
}

template <typename T>
V4<T> v4(T x, V2<T> yz, T w)
{
  V4<T> result;
  result.x = x;
  result.y = yz.v[0];
  result.z = yz.v[1];
  result.w = w;
  return result;
}

template <typename T, typename U, typename V, typename W>
V4<T> v4(U x, V2<V> yz, W w)
{
  V4<T> result;
  result.x = x;
  result.y = yz.v[0];
  result.z = yz.v[1];
  result.w = w;
  return result;
}

template <typename T>
V4<T> v4(V3<T> xyz, T w)
{
  V4<T> result;
  result.x = xyz.v[0];
  result.y = xyz.v[1];
  result.z = xyz.v[2];
  result.w = w;
  return result;
}

template <typename T, typename U, typename V>
V4<T> v4(V3<U> xyz, V w)
{
  V4<T> result;
  result.x = xyz.v[0];
  result.y = xyz.v[1];
  result.z = xyz.v[2];
  result.w = w;
  return result;
}

template <typename T>
V4<T> v4(T x, V3<T> yzw)
{
  V4<T> result;
  result.x = x;
  result.y = yzw.v[0];
  result.z = yzw.v[1];
  result.w = yzw.v[2];
  return result;
}

template <typename T, typename U, typename V>
V4<T> v4(U x, V3<V> yzw)
{
  V4<T> result;
  result.x = x;
  result.y = yzw.v[0];
  result.z = yzw.v[1];
  result.w = yzw.v[2];
  return result;
}

template <typename T, typename U>
V4<T> v4(V4<U> v)
{
  V4<T> result;
  result.x = v.x;
  result.y = v.y;
  result.z = v.z;
  result.w = v.w;
  return result;
}

template <typename T>
bool operator ==(const V4<T> &a, const V4<T> &b)
{
  return (a.x == b.x) &&
         (a.y == b.y) &&
         (a.z == b.z) &&
         (a.w == b.w);
}

template <typename T>
bool operator !=(const V4<T> &a, const V4<T> &b)
{
  return !(a == b);
}

template <typename T>
V4<T> operator +(const V4<T> &a, const V4<T> &b)
{
  V4<T> result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  result.z = a.z + b.z;
  result.w = a.w + b.w;
  return result;
}

template <typename T>
V4<T> &operator +=(V4<T> &a, const V4<T> &b)
{
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  a.w += b.w;
  return a;
}

template <typename T>
V4<T> operator -(const V4<T> &a, const V4<T> &b)
{
  V4<T> result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  result.z = a.z - b.z;
  result.w = a.w - b.w;
  return result;
}

template <typename T>
V4<T> operator -=(V4<T> &a, const V4<T> &b)
{
  a.x -= b.x;
  a.y -= b.y;
  a.z -= b.z;
  a.w -= b.w;
  return a;
}

template <typename T>
V4<T> operator -(const V4<T> &v)
{
  V4<T> result;
  result.x = -v.x;
  result.y = -v.y;
  result.z = -v.z;
  result.w = -v.w;
  return result;
}

template <typename T>
V4<T> operator *(V4<T> v, T s)
{
  V4<T> result;
  result.x = v.x * s;
  result.y = v.y * s;
  result.z = v.z * s;
  result.w = v.w * s;
  return result;
}

template <typename T>
V4<T> operator *(T s, V4<T> v)
{
  return v * s;
}

template <typename T>
V4<T> &operator *=(V4<T> &v, const T &s)
{
  v.x *= s;
  v.y *= s;
  v.z *= s;
  v.w *= s;
  return v;
}

template <typename T>
V4<T> operator /(V4<T> v, T s)
{
  V4<T> result;
  result.x = v.x / s;
  result.y = v.y / s;
  result.z = v.z / s;
  result.w = v.w / s;
  return result;
}

template <typename T>
V4<T> &operator /=(V4<T> &v, const T &s)
{
  v.x /= s;
  v.y /= s;
  v.z /= s;
  v.w /= s;
  return v;
}

template <typename T>
T dot(const V4<T> a, const V4<T> b)
{
  return (a.x * b.x) +
         (a.y * b.y) +
         (a.z * b.z) +
         (a.w * b.w);
}

template <typename T>
V4<T> hadamard(const V4<T> a, const V4<T> b)
{
  return v4(a.x * b.x,
            a.y * b.y,
            a.z * b.z,
            a.w * b.w);
}
