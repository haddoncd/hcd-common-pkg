#ifdef args_hpp
#error Multiple inclusion
#endif
#define args_hpp

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif



// Terminology, as used here:
//   Argument   (arg):     an argument passed to the application, in general
//   Option     (opt):     either a long or short option
//   Parameter  (param):   an "argument" to an option

// Short options follow a single minus, and consist of a single char. Eg. "-o"
// - May have one parameter, which follows the option after a space, ie.
//   "-o foo".
// - Multiple can be specified together, ie. "-a -b -c" and "-abc" are
//   equivalent.
// - When multiple short options are specifed together, only the last can have a
//   parameter, ie. "-abc blah" is equivalent to "-ab -c blah"

// Long options follow a double minus, and are descriptive, with minuses
// separating words. Eg. "--long-option"
// - May have one parameter, which follows the option after an equals, ie.
//   "--long-option=foo"

namespace args
{
  namespace Type
  {
    enum Enum
    {
      NON_OPT,
      SHORT_OPTS,
      LONG_OPT,
      END_OF_OPTS,
      ENUM_COUNT
    };
  }

  Type::Enum typeOf(Str arg);

  struct ArrayEntry
  {
    Type::Enum type;
    Str        arg;
  };

  struct Iterator;

  struct Array
  {
    u32       count;
    ArrayEntry args[0];

    NO_COPY_OR_MOVE(Array)

    static size_t sizeWith(u32 count);

    void init(u32 argc, char **argv);
    Iterator start();
    Iterator skipArg0();
  };

  struct Iterator
  {
    Array *array;
    u32    argIndex;
    u32    shortOptIndex;

    bool valid();
    void step();
    Str getParam();
    Type::Enum getType();
    Str getArg();
    bool matchOpt(Str long_opt, char short_opt = '\0');
  };
}

#define args_Array_localAllocAndInit(p, argc, argv)              \
do                                                               \
{                                                                \
  u32 count = argc;                                              \
  p = (::args::Array *) _alloca(::args::Array::sizeWith(count)); \
  p->count = count;                                              \
  p->init(count, argv);                                          \
}                                                                \
while (false)
