#ifdef Array_hpp
#error Multiple inclusion
#endif
#define Array_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Slice_hpp
#error "Please include Slice.hpp before this file"
#endif

#ifndef HeapAllocator_hpp
#error "Please include HeapAllocator.hpp before this file"
#endif

namespace array
{
  constexpr u32 MIN_ALLOCATED_CAPACITY = 16;
}


template <typename T>
struct Array
{
  struct Buffer
  {
    u32 capacity;
    T   data[];

    static size_t sizeWith(u32 capacity)
    {
      return offsetOf(Buffer, data[capacity]);
    }

    NO_COPY_OR_MOVE(Buffer)

    void setSize(u32 capacity)
    {
      this->capacity = capacity;
    }
  };

  HeapAllocator *allocator = STD_HEAP;
  Buffer        *buffer    = 0;
  u32            count     = 0;

  T &operator[](u32 idx)
  {
    assert(idx <= count);
    assert(count <= capacity());

    return buffer->data[idx];
  }

  operator Slice<T>()
  {
    return Slice<T>(count, count ? buffer->data : 0);
  }

  Slice<T> slice(size_t start, size_t span)
  {
    Slice<T> slice = *this;
    return slice.slice(start, span);
  }

  u32 chooseCapacity(u32 min_size)
  {
    return ceilPowerOfTwo(maximum(min_size, array::MIN_ALLOCATED_CAPACITY));
  }

  void resize(u32 new_capacity)
  {
    assert(count <= capacity());
    assert(count <= new_capacity);

    if (capacity() == new_capacity) return;

    Buffer *new_buffer;

    if (new_capacity)
    {
      new_buffer = allocUninitWith<Buffer>(allocator, new_capacity);
      if (count)
      {
        memcpy(new_buffer->data, buffer->data, count * sizeof(T));
      }
    }
    else
    {
      new_buffer = 0;
    }

    if (buffer) allocator->free(buffer);

    buffer = new_buffer;
  }

  void ensureCapacity(u32 new_capacity)
  {
    if (capacity() < new_capacity)
    {
      resize(chooseCapacity(new_capacity));
    }
  }

  void free()
  {
    clear();
    resize(0);
  }

  void clear()
  {
    count = 0;
  }

  u32 capacity()
  {
    return buffer ? buffer->capacity : 0;
  }

  T *last()
  {
    return count ? &buffer->data[count-1] : 0;
  }

  T *appendUninit()
  {
    ensureCapacity(count+1);
    ++count;
    return &buffer->data[count-1];
  }

  Slice<T> appendManyUninit(u32 num)
  {
    u32 old_count = count;
    ensureCapacity(count + num);
    count += num;
    return slice(old_count, num);
  }

  T *append()
  {
    T *element = appendUninit();
    zeroStruct(element);
    return element;
  }

  Slice<T> appendMany(u32 num)
  {
    Slice<T> slice = appendManyUninit(num);
    zeroArray(slice.data, num);
    return slice;
  }

  T *append(T value)
  {
    T *element = appendUninit();
    *element = value;
    return element;
  }

  Slice<T> append(Slice<T> values)
  {
    Slice<T> slice = appendManyUninit(num);
    memcpy(slice.data, values.data, slice.sizeInBytes());
    return slice;
  }

  Slice<T> insertManyUninit(u32 idx, u32 num)
  {
    assert(idx <= count);
    u32 number_to_slide = count - idx;

    if (count + num <= capacity())
    {
      if (number_to_slide)
      {
        memmove(
          &buffer->data[idx + num],
          &buffer->data[idx],
          number_to_slide * sizeof(T)
        );
      }

      count += num;
    }
    else
    {
      u32 new_capacity = chooseCapacity(count + num);
      assert(count <= capacity());

      Buffer *new_buffer;

      if (new_capacity)
      {
        new_buffer = allocUninitWith<Buffer>(allocator, new_capacity);
        if (idx)
        {
          memcpy(new_buffer->data, buffer->data, idx * sizeof(T));
        }
        if (count - idx)
        {
          memcpy(&new_buffer->data[idx + num], &buffer->data[idx], (count - idx) * sizeof(T));
        }
      }
      else
      {
        new_buffer = 0;
      }

      if (buffer) allocator->free(buffer);

      buffer = new_buffer;

      count += num;
    }
  }

  Slice<T> insertMany(u32 idx, u32 num)
  {
    Slice<T> slice = insertManyUninit(idx, num);
    zeroArray(slice.data, num);
    return slice;
  }

  Slice<T> insertMany(u32 idx, Slice<T> values)
  {
    Slice<T> slice = insertManyUninit(idx, values.size);
    memcpy(slice.data, values.data, values.size * sizeof(T));
    return slice;
  }

  T *insertUninit(u32 idx)
  {
    T *elem = insertManyUninit(idx, 1).data;
    return elem;
  }

  T *insert(u32 idx)
  {
    T *elem = insertMany(idx, 1).data;
    return elem;
  }

  T *insert(u32 idx, T value)
  {
    T *elem = insertManyUninit(idx, 1).data;
    *elem = value;
    return elem;
  }

  void swapRemove(u32 idx, u32 count_to_remove = 1)
  {
    assert(idx + count_to_remove <= count);
    if (!count_to_remove) return;

    u32 first_idx_to_swap_from = maximum(idx + count_to_remove, count - count_to_remove);
    u32 number_to_swap = count - first_idx_to_swap_from;

    if (number_to_swap)
    {
      memcpy(
        &buffer->data[idx],
        &buffer->data[first_idx_to_swap_from],
        number_to_swap * sizeof(T)
      );
    }

    count -= count_to_remove;
  }

  void slideRemove(u32 idx, u32 count_to_remove = 1)
  {
    assert(idx + count_to_remove <= count);
    if (!count_to_remove) return;

    u32 first_idx_to_slide_from = idx + count_to_remove;
    u32 number_to_slide = count - first_idx_to_slide_from;

    if (number_to_slide)
    {
      memmove(
        &buffer->data[idx],
        &buffer->data[first_idx_to_slide_from],
        number_to_slide * sizeof(T)
      );
    }

    count -= count_to_remove;
  }
};
