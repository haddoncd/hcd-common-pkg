#ifdef print_cpp
#error Multiple inclusion
#endif
#define print_cpp

#ifndef print_hpp
#include "print.hpp"
#endif


bool print(FILE *out, Str str)
{
  return fwrite(str.data, sizeof(char), str.length, out) == str.length;
}

bool print(FILE *out, char c)
{
  return fputc(c, out) == c;
}

DefinePrintfType(u32, u)
DefinePrintfType(i32, i)
DefinePrintfType(u64, I64u)
DefinePrintfType(i64, I64i)
DefinePrintfType(f64, g)
