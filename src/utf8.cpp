#ifdef utf8_cpp
#error Multiple inclusion
#endif
#define utf8_cpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef utf8_hpp
#include "utf8.hpp"
#endif


utf8_Codepoint utf8_codepoint(u32 codepoint)
{
  utf8_Codepoint result = {0};

  if (0x10ffff < codepoint) codepoint = 0xfffd;

  BREAKABLE_START
  {
    if (codepoint < 0x80)
    {
      result.bytes[0] = (u8) (codepoint & 0x7f);
      result.byteCount = 1;
      break;
    }

    u8 byte_a = (u8) ((codepoint & 0x3f) | 0x80);

    if (codepoint < 0x0800)
    {
      result.bytes[0] = (u8) (((codepoint >> 6) & 0x1f) | 0xc0);
      result.bytes[1] = byte_a;
      result.byteCount = 2;
      break;
    }

    u8 byte_b = (u8) (((codepoint >> 6) & 0x3f) | 0x80);

    if (codepoint < 0x010000)
    {
      result.bytes[0] = (u8) (((codepoint >> 12) & 0x0f) | 0xe0);
      result.bytes[1] = byte_b;
      result.bytes[2] = byte_a;
      result.byteCount = 3;
      break;
    }

    u8 byte_c = (u8) (((codepoint >> 12) & 0x3f) | 0x80);

    result.bytes[0] = (u8) (((codepoint >> 18) & 0x07) | 0xf0);
    result.bytes[1] = byte_c;
    result.bytes[2] = byte_b;
    result.bytes[3] = byte_a;
    result.byteCount = 3;
  }
  BREAKABLE_END

  return result;
}
