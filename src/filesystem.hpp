#ifdef filesystem_hpp
#error Multiple inclusion
#endif
#define filesystem_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef File_hpp
#error "Please include File.hpp before this file"
#endif



namespace filesystem
{
  // load funcs return 0 on failure
  // non-zero returned pointer can be freed

  template <typename A>
  File *load(A allocator, const char *path)
  {
    File *result = 0;

    BREAKABLE_START
    {
      FILE *file = fopen(path, "rb");

      if (!file) break;

      if (fseek(file, 0L, SEEK_END) != 0) break;

      u64 size = ftell(file);

      if (fseek(file, 0L, SEEK_SET) != 0) break;

      File *loaded_file = allocUninitSized<File>(allocator, File::sizeWith(size));

      loaded_file->size = fread(loaded_file->data, 1, size, file);

      if (loaded_file->size != size)
      {
        free(loaded_file);
        break;
      }

      // Set null terminator
      loaded_file->data[size] = 0;

      result = loaded_file;

      fclose(file);
    }
    BREAKABLE_END

    return result;
  }

  bool exists(const char *path);

  u64 lastWriteTime(const char *path);
}
