#ifdef Str_checks_hpp
#error Multiple inclusion
#endif
#define Str_checks_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef common_checks_hpp
#error "Please include common_checks.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#define CHECK_STR(expr, expected)                                \
  do                                                             \
  {                                                              \
    Str actual = expr;                                           \
    if (actual != expected)                                      \
    {                                                            \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"                   \
             "      " #expr " => \"",                            \
             __FUNCTION__);                                      \
      fwrite(actual.data, sizeof(char), actual.length, stdout);  \
      puts("\" != " #expected);                                  \
      checks_passed = false;                                     \
    }                                                            \
  }                                                              \
  while (false)

#define CHECK_STR_EMPTY(expr)                                    \
  do                                                             \
  {                                                              \
    Str actual = expr;                                           \
    if (actual.length != 0)                                      \
    {                                                            \
      printf("FAIL: %s(" LINE_NUM_STRING ")\n"                   \
             "      " #expr " => \"",                            \
             __FUNCTION__);                                      \
      fwrite(actual.data, sizeof(char), actual.length, stdout);  \
      puts("\" != \"\"");                                        \
      checks_passed = false;                                     \
    }                                                            \
  }                                                              \
  while (false)
