#ifdef List_hpp
#error Multiple inclusion
#endif
#define List_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif


template <typename T>
struct List
{
  struct Link;

  struct Chain
  {
    Link *first;
    Link *last;

    bool isEmpty()
    {
      return first == 0;
    }
  };

  struct Link
  {
    Link *prev;
    Link *next;
    T data;

    void append(Chain chain)
    {
      insertBetween(this, this->next, chain);
    }

    void prepend(Chain chain)
    {
      insertBetween(this->prev, this, chain);
    }

    void append(Link *item)
    {
      append(Chain{item, item});
    }

    void prepend(Link *item)
    {
      prepend(Chain{item, item});
    }

    void remove()
    {
      Link *presequent = this->prev;
      Link *subsequent = this->next;

      presequent->next = subsequent;
      subsequent->prev = presequent;
      this->prev = this->next = this;
    }

    void remove(List<T> *free_list)
    {
      remove();
      free_list->append(this);
    }

    template <typename... Us>
    static size_t sizeWith(Us &&... args)
    {
      return offsetOf(List<T>::Link, data) + T::sizeWith(std::forward<Us>(args)...);
    }

    template <typename... Us>
    void setSize(Us &&... args)
    {
      data.setSize(std::forward<Us>(args)...);
    }
  };

  static void insertBetween(Link *presequent, Link *subsequent, Chain chain)
  {
    presequent->next = chain.first;
    chain.first->prev = presequent;
    chain.last->next = subsequent;
    subsequent->prev = chain.last;
  }

  struct Loop
  {
    Link *first;

    operator Chain()
    {
      if (isEmpty()) return Chain{0, 0};
      else           return Chain{first, first->prev};
    }

    bool isEmpty()
    {
      return first == 0;
    }
  };

  static Loop remove(Chain chain)
  {
    Link *presequent = chain.first->prev;
    Link *subsequent = chain.last->next;

    chain.first->prev = chain.last;
    chain.last->next = chain.first;

    presequent->next = subsequent;
    subsequent->prev = presequent;

    return Loop{chain.first};
  }

  static void remove(Chain chain, List<T> *free_list)
  {
    Link *presequent = chain.first->prev;
    Link *subsequent = chain.last->next;

    presequent->next = subsequent;
    subsequent->prev = presequent;

    free_list->append(chain);
  }

  struct Sentinel
  {
    Link *prev;
    Link *next;
  };

  static_assert(offsetOf(Sentinel, prev) == offsetOf(Link, prev), "List sentinel incompatibility!");
  static_assert(offsetOf(Sentinel, next) == offsetOf(Link, next), "List sentinel incompatibility!");

  Sentinel _sentinel;
  Link *sentinel()
  {
    return (Link *)&_sentinel;
  }

  void init()
  {
    _sentinel.next = _sentinel.prev = sentinel();
  }

  void init(Chain chain)
  {
    insertBetween(sentinel(), sentinel(), chain);
  }

  List()
  {
    this->init();
  }

  List(Chain chain)
  {
    this->init(chain);
  }

  NO_COPY_OR_MOVE(List)

  Link *first()
  {
    return _sentinel.next;
  }

  Link *last()
  {
    return _sentinel.prev;
  }

  bool isEmpty()
  {
    return _sentinel.next == sentinel();
  }

  u32 count()
  {
    u32 result = 0;

    for (auto *link = first();
         link != sentinel();
         link = link->next)
    {
      ++result;
    }

    return result;
  }

  void prepend(Chain chain)
  {
    sentinel()->append(chain);
  }

  void append(Chain chain)
  {
    sentinel()->prepend(chain);
  }

  void prepend(Link *item)
  {
    sentinel()->append(item);
  }

  void append(Link *item)
  {
    sentinel()->prepend(item);
  }

  Loop removeAll()
  {
    return remove(Chain{first(), last()});
  }

  void removeAll(List<T> *free_list)
  {
    remove(Chain{first(), last()}, free_list);
  }

  Link *popFirst()
  {
    Link *result = 0;

    if (!isEmpty())
    {
      result = _sentinel.next;
      result->remove();
    }

    return result;
  }

  Link *popLast()
  {
    Link *result = 0;

    if (!isEmpty())
    {
      result = _sentinel.prev;
      result->remove();
    }

    return result;
  }

  // NB. Treat returned link as uninitialised memory.
  template <typename A>
  Link *getRecycledLink(A allocator)
  {
    Link *result = popLast();

    if (!result) result = allocUninit<Link>(allocator);

    return result;
  }

  void sortedMerge(Loop that_loop, bool (*is_sorted)(T*, T*))
  {
    List<T> that(that_loop);
    Link *this_link = this->first();

    while ( !that.isEmpty() )
    {
      Link *insert_first = that.first();

      while ( this_link != this->sentinel() &&
              is_sorted(&this_link->data, &insert_first->data) )
      {
        this_link = this_link->next;
      }

      Link *insert_last = insert_first;

      while ( insert_last->next != that.sentinel() &&
              ( this_link == this->sentinel() ||
                !is_sorted(&this_link->data, &insert_last->next->data) ) )
      {
        insert_last = insert_last->next;
      }

      this_link->prepend(remove(Chain{insert_first, insert_last}));
      if (this_link != this->sentinel()) this_link = this_link->next;
    }
  }

  Loop popFirstTwoToTheNSortedBy(u32 n, bool (*is_sorted)(T*, T*))
  {
    Loop result;

    if (isEmpty())
    {
      result.first = 0;
    }
    else if (first() == last())
    {
      result = removeAll();
    }
    else if (first()->next == last())
    {
      result = removeAll();
      if (!is_sorted(&result.first->data, &result.first->next->data))
      {
        result.first = result.first->next;
      }
    }
    else if (n == 0)
    {
      result.first = popFirst();
    }
    else if (n == 1)
    {
      result = remove(Chain{first(), first()->next});
      if (!is_sorted(&result.first->data, &result.first->next->data))
      {
        result.first = result.first->next;
      }
    }
    else
    {
      Loop a = popFirstTwoToTheNSortedBy(n-1, is_sorted);
      Loop b = popFirstTwoToTheNSortedBy(n-1, is_sorted);

      if (a.isEmpty())
      {
        result = b;
      }
      else if (b.isEmpty())
      {
        result = a;
      }
      else
      {
        List<T> a_list(a);
        a_list.sortedMerge(b, is_sorted);
        result = a_list.removeAll();
      }
    }

    return result;
  }

  void sort(bool (*is_sorted)(T*, T*))
  {
    if (isEmpty() || first() == last()) return;

    if (first()->next == last()->prev)
    {
      if (!is_sorted(&first()->data, &first()->next->data))
      {
        append(popFirst());
      }
      return;
    }

    List<T> unsorted(remove(Chain{first()->next->next, last()}));
    if (!is_sorted(&first()->data, &first()->next->data))
    {
      append(popFirst());
    }
    u32 sorted_count_log2 = 1;

    while(!unsorted.isEmpty())
    {
      Loop tmp = unsorted.popFirstTwoToTheNSortedBy(sorted_count_log2++, is_sorted);
      if (!tmp.isEmpty()) sortedMerge(tmp, is_sorted);
    }
  }

  struct IteratorWithCircularPrevAndNext
  {
    Link *sentinel;
    Link *current;
    Link *next;
    Link *prev;

    void init(List<T> *list)
    {
      sentinel = list->sentinel();
      current = 0;
      next = 0;
      prev = 0;
    }

    IteratorWithCircularPrevAndNext(List<T> *list)
    {
      this->init(list);
    }

    bool step()
    {
      if (!current)
      {
        current = sentinel->next;
        if (current == sentinel)
        {
          current = 0;
          next = 0;
          prev = 0;
        }
        else
        {
          next = current->next;
          if (next == sentinel)
          {
            prev = next = current;
          }
          else
          {
            prev = sentinel->prev;
          }
        }
      }
      else if (current == next || current->next == sentinel)
      {
        current = 0;
        next = 0;
        prev = 0;
      }
      else
      {
        prev = current;
        current = next;
        next = next->next;
        if (next == sentinel) next = next->next;
      }

      return current != 0;
    }
  };
};

#define forEachLink(it, list) for (auto *it = (list).first(); it != (list).sentinel(); it = it->next)

#define forEachLinkData(it, list) for (auto *it##_link = (list).first(); auto *it = (it##_link == (list).sentinel() ? 0 : &it##_link->data); it##_link = it##_link->next)
