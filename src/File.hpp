#ifdef File_hpp
#error Multiple inclusion
#endif
#define File_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif


struct File
{
  u64 size;
  u8  data[];

  static size_t sizeWith(u64 count);

  NO_COPY_OR_MOVE(File)

  operator ZStr() const;
  operator Str() const;
  void mungeCRLF();
};
