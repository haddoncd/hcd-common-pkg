#ifdef File_cpp
#error Multiple inclusion
#endif
#define File_cpp

#ifndef File_hpp
#include "File.hpp"
#endif

#ifndef StrBuf_hpp
#error "Please include StrBuf.hpp before this file"
#endif


// File may be text, so always include a null terminator byte
size_t File::sizeWith(u64 count)
{
  return offsetOf(File, data[count + 1]);
}

File::operator ZStr() const
{
  return ZStr{size, (char *)data};
}

File::operator Str() const
{
  return Str{size, (char *)data};
}

void File::mungeCRLF()
{
  Str remaining = *this;
  StrBuf::Fixed<0> *buf = (StrBuf::Fixed<0> *)this;
  u64 buf_capacity = size;
  buf->length = 0;

  while (remaining.length)
  {
    StrBuf::Generic::append(buf, buf_capacity, remaining.chop({ "\r\n"_s, "\n"_s, "\r"_s }));
    StrBuf::Generic::append(buf, buf_capacity, '\n');
  }

  bool ok = StrBuf::Generic::nullTerminate(buf, buf_capacity);
  assert(ok);
}
