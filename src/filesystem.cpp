#ifdef filesystem_cpp
#error Multiple inclusion
#endif
#define filesystem_cpp

#ifndef filesystem_hpp
#include "filesystem.hpp"
#endif

// TODO: Non Win32 platforms
#ifndef _WINDOWS_
#error "Please include Windows.h before this file"
#endif



namespace filesystem
{
  bool exists(const char *path)
  {
    DWORD attributes = GetFileAttributes(path);

    return (attributes != INVALID_FILE_ATTRIBUTES &&
           !(attributes & FILE_ATTRIBUTE_DIRECTORY));
  }

  u64 lastWriteTime(const char *path)
  {
    u64 result = 0;
    WIN32_FILE_ATTRIBUTE_DATA attributes;

    if (GetFileAttributesEx(path, GetFileExInfoStandard, &attributes) != 0)
    {
      ULARGE_INTEGER time;
      time.LowPart = attributes.ftLastWriteTime.dwLowDateTime;
      time.HighPart = attributes.ftLastWriteTime.dwHighDateTime;
      result = time.QuadPart;
    }

    return result;
  }
}
