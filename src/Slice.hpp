#ifdef Slice_hpp
#error Multiple inclusion
#endif
#define Slice_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

template <typename T>
struct Slice
{
  size_t  size;
  T      *data;

  constexpr Slice(nullptr_t unused):
    size(0),
    data(nullptr)
  {}

  explicit Slice(T *data):
    size(1),
    data(data)
  {}

  Slice(size_t size, T *data):
    size(size),
    data(data)
  {}

  template <size_t N>
  Slice(T (&array)[N]):
    size(N),
    data(array)
  {}

  T &operator[](size_t idx)
  {
    assert(idx <= size);

    return data[idx];
  }

  Slice<T> slice(size_t start, size_t span)
  {
    assert(start + span <= size);
    if (span == 0) return empty();
    else return Slice{ span, data + start };
  }

  size_t sizeInBytes()
  {
    return size * sizeof(T);
  }
};
